# -*- coding: utf-8 -*-
{
    'name': "Sổ liên lạc điện tử",
    'summary': "Quản lý  thông tin trường học, giáo viên, học sinh, phụ huynh",
    'description': "Quản lý  thông tin trường học, giáo viên, học sinh, phụ huynh",

    'author': "BMSGROUP GLOBAL",
    'website': "http://www.bmsgroupglobal.com",
    'category': 'BMS APP',
    'version': '1.1',
    "application": True,
    "installable": True,
    # any module necessary for this one to work correctly
    'depends': ['base',
                'hr',
                'web',
                'bms_localization',
                'inputmask_widget'
                ],
    'qweb': [
        'static/src/xml/widget.xml',
    ],

    # always loaded
    'data': [
        'data/cron.xml',
        'data/sms_config_data.xml',
        'data/config_more_information.xml',
        'security/bms_school_report_security.xml',
        'security/ir.model.access.csv',
        'views/more_infomation_view.xml',
        'views/school_management_view.xml',
        'views/group_of_class_view.xml',
        'views/study_class_view.xml',
        'views/teacher_management_view.xml',
        'views/sehoot_year_view.xml',
        'views/student_management_view.xml',
        'views/student_history_view.xml',
        'views/bms_idea_box_view.xml',
        'wizard/views/wizard_parent_idea_box_view.xml',
        'views/res_partner_view.xml',
        'views/group_student_view.xml',
        'views/sms_management_view.xml',
        'views/sms_teacher_per_month_view.xml',
        'views/sms_config_view.xml',
        'views/bms_sms_compose_views.xml',
        'views/upload_images_view.xml',
        'views/mobile_blog_view.xml',
        'views/principal_sms_view.xml',
        'views/send_message_view.xml',
        'views/menu_view.xml',
    ],
}
