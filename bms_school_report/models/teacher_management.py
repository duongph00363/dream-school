# -*- coding: utf-8 -*-
from odoo import fields, models, api


class BmsTeacherManagement(models.Model):
    _inherit = 'hr.employee'

    street = fields.Char(string='Đường')
    state_id = fields.Many2one('res.country.state',
                               string='Tỉnh/Thành phố',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district', string='Quận/Huyện')
    ward_id = fields.Many2one('res.country.ward', string='Phường/xã')
    school_id = fields.Many2one('res.company', required=True)
    year_working = fields.Integer()
    subject = fields.Char()
    study_class_ids = fields.Many2many('bms.study.class',
                                       'teacher_management_study_class_rel',
                                       'teacher_id',
                                       'study_class_id')
    study_class_id = fields.Many2one('bms.study.class', compute='_load_class', string='Chủ nhiệm lớp')

    birthday = fields.Date('Date of Birth', groups='base.group_user')
    identification_id = fields.Char(string='Identification No',
                                    groups='base.group_user')
    teacher_id_many = fields.Many2many('principal.sms','teacher_id_many_rel','hr_employee_id', 'principal_id', string ='Hieu truong')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], groups='base.group_user')
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status', groups='base.group_user')
    bank_account_id = fields.Many2one('res.partner.bank',
                                      string='Bank Account Number',
                                      domain="[('partner_id', '=', address_home_id)]",
                                      help='Employee bank salary account',
                                      groups='base.group_user')

    head_master_ids = fields.One2many('res.company', 'head_master', string='Các hiệu trưởng')
    management_system_ids = fields.One2many('res.company', 'management_system', string='Các quản lý hệ thống của trường')
    homeroom_teacher_ids = fields.One2many('bms.study.class', 'homeroom_teacher', string='Các giáo viên chủ nhiệm')

    is_head_master = fields.Boolean(compute='_compute_is_head_master', store=True)
    is_management_system = fields.Boolean(compute='_compute_is_management_system', store=True)
    is_homeroom_teacher = fields.Boolean(compute='_compute_is_homeroom_teacher', store=True)

    sms_remain = fields.Integer(string='Số tin nhắn còn lại')
    user_id_email = fields.Char(string='Thông tin đăng nhập', store=True, related='user_id.email')

    @api.multi
    def _load_class(self):
        for s in self:
            s.study_class_id = self.env['bms.study.class'].sudo().search([('homeroom_teacher', '=', s.id)])

    @api.multi
    @api.depends('head_master_ids')
    def _compute_is_head_master(self):
        for s in self:
            s.is_head_master = False
            if s.head_master_ids:
                s.is_head_master = True

    @api.multi
    @api.depends('management_system_ids')
    def _compute_is_management_system(self):
        for s in self:
            s.is_management_system = False
            if s.management_system_ids:
                s.is_management_system = True

    @api.multi
    @api.depends('homeroom_teacher_ids')
    def _compute_is_homeroom_teacher(self):
        for s in self:
            s.is_homeroom_teacher = False
            if s.homeroom_teacher_ids:
                s.is_homeroom_teacher = True

    @api.multi
    def create_user(self):
        user_value = {
            'name': self.name,
            'login': self.work_email,
            'password': '123456',
            'email': self.work_email,
            'company_ids': [(6, False, [self.school_id.id])],
            'company_id': self.school_id.id,
            'groups_id': [(6, False, [self.env.ref(
                'bms_school_report.group_user_teacher').id])],
        }
        user_id = self.env['res.users'].sudo().create(user_value)
        self.write({'user_id': user_id.id})
        return user_id


    @api.onchange('company_id')
    def _onchange_company(self):
        return False
