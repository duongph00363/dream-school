# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo import fields, models, api, _
import pytz
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)


class SMSManagement(models.Model):
    _name = 'sms.management'
    _description = 'SMS Management'
    _rec_name = 'id'

    #lấy giá trị default của giáo viên
    def _teacher_default(self):
        # is_management = self.env.user.has_group(
        #     'bms_school_report.group_management_user')
        # if is_management:
        #     return False
        # is_teacher = self.env.user.has_group(
        #     'bms_school_report.group_user_teacher')
        # if is_teacher:
        employees = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        return employees and employees[0] or False
        # return False
    def _school_default(self):
        schools = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if schools and schools.school_id:
            return schools.school_id
        return False
    def _year_default(self):
        schools = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if schools and schools.school_id:
            year = self.env['bms.sehoot.year'].search([('school_id', '=', schools.school_id.id), ('active', '=', True)])
            if year:
                return year
        return False
    def _class_default(self):
        employees = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if employees:
            class_obj = self.env['bms.study.class'].search([('homeroom_teacher', '=', employees.id)])
            return class_obj
        return False

    # xử lý xác định user đăng nhập là giáo viên
    def _is_user_management_default(self):
        res = self.env.user.has_group(
            'bms_school_report.group_management_user') or False
        return res

    def _sms_teacher_per_month_default(self):
        date_now = datetime.now()
        year_now = date_now.year
        month_now = str(date_now.month)
        is_teacher = self.env.user.has_group(
            'bms_school_report.group_user_teacher')
        is_management = self.env.user.has_group(
            'bms_school_report.group_management_user')
        if is_teacher or is_management:
            employees = self.env['hr.employee'].search(
                [('user_id', '=', self.env.user.id)])
        if employees:
            employees_id = employees and employees[0] or False
            if employees_id:
                domain = [
                          ('teacher_id', '=', employees_id.id),
                          ('year', '=', year_now),
                          ('month', '=', month_now)]
                sms_teacher_per_month_id = self.env['sms.teacher.per.month'].search(domain)
                return sms_teacher_per_month_id and sms_teacher_per_month_id[0] or False
            return False

    name = fields.Char()
    school_id = fields.Many2one('res.company',store=True, required=True, default=_school_default)
    date_sending = fields.Datetime(required=True, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    sehoot_year_id = fields.Many2one('bms.sehoot.year', required=True, default=_year_default)
    study_class_id = fields.Many2one('bms.study.class', string='Lớp', required=True, default=_class_default)
    teacher_id = fields.Many2one('hr.employee', string='Giáo viên', default=_teacher_default, required=True)
    student_ids = fields.Many2many('bms.student.management',
                                   'sms_management_student_management_rel',
                                   'sms_management_id', 'student_id',
                                   string='Học sinh')
    is_send_all = fields.Boolean()
    group_student_ids = fields.Many2many('group.student',
                                         'sms_management_group_student_rel',
                                         'sms_management_id', 'group_student_id',
                                         string='Nhóm học sinh')
    # sms_teacher_per_month_id = fields.Many2one('sms.teacher.per.month', compute='_computed_sms_teacher_per_month', readonly=True)
    sms_teacher_per_month_id = fields.Many2one('sms.teacher.per.month', readonly=True, default=_sms_teacher_per_month_default)
    sms_numbers_limit = fields.Integer(related='sms_teacher_per_month_id.sms_numbers_limit', store=True)
    sms_remain = fields.Integer(compute='_compute_sms_remain', store=True)

    is_homeroom_teacher = fields.Boolean(compute='_compute_is_homeroom_teacher')

    # từ nội dung tin nhắn tính ra số SMS gửi cho mỗi học sinh
    sms_sending_per_student = fields.Integer(compute='_compute_sms_sending_per_student', store=True)

    # Tổng số SMS trong lần gửi
    sms_sending_total_number = fields.Integer(compute='_compute_sms_total', store=True)
    accent_vietnamese = fields.Selection([('no_accent', 'Tiếng Việt không dấu'),
                                          ('accent', 'Tiếng Việt có dấu')], default='no_accent')
    # sms_detail_no_accent = fields.Text()
    # sms_detail_accent = fields.Text()
    message = fields.Text(compute='_compute_message', store=True)
    # message_compute = fields.Text(compute='_compute_message', inverse='_set_message')
    message_no_accent = fields.Text()
    message_accent = fields.Text()



    #xử lý xác định user đăng nhập là giáo viên
    is_user_management = fields.Boolean(compute='_compute_is_user_management',
                                     default=_is_user_management_default)

    #tạo state cho quy trình gửi tín nhắn
    state = fields.Selection([('draft', 'Soạn SMS'),
                              ('processing', 'Đang gửi tin'),
                              ('error', 'Lỗi'),
                              ('done', 'Hoàn thành'),
                              ('cancel', 'Hủy bỏ')],
                             required=True, readonly=True,
                             default='draft')

    sms_log_ids = fields.One2many('bms.sms.log', 'sms_management_id', string='SMS Log')

    #loai sms_numbers_limit ra khoi dict create, tranh ghi lai db sms.teacher.per.month
    @api.model
    def create(self, vals):
        if 'sms_numbers_limit' in vals.keys():
            del vals['sms_numbers_limit']
        return super(SMSManagement, self).create(vals)


    @api.model
    def _compute_is_user_management(self):
        is_management = self.env.user.has_group(
            'bms_school_report.group_management_user')
        self.is_user_management = is_management

    @api.multi
    @api.depends('teacher_id')
    def _compute_is_homeroom_teacher(self):
        for s in self:
            if s.teacher_id.is_homeroom_teacher:
                s.is_homeroom_teacher = True
            else:
                s.is_homeroom_teacher = False

    @api.multi
    def change_utc_to_local_datetime(self, source_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.strptime(source_date, '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    # @api.multi
    # @api.depends('date_sending', 'school_id', 'teacher_id')
    # def _computed_sms_teacher_per_month(self):
    #     for s in self:
    #         date_sending = s.change_utc_to_local_datetime(s.date_sending)
    #         date_sending = datetime.strptime(date_sending, '%Y-%m-%d %H:%M:%S')
    #         domain = []
    #         if s.school_id:
    #             school_id = s.school_id.id
    #             domain.append(('school_id', '=', school_id))
    #         if s.teacher_id:
    #             teacher_id = s.teacher_id.id
    #             domain.append(('teacher_id', '=', teacher_id))
    #         year = date_sending.year
    #         domain.append(('year', '=', year))
    #         month = date_sending.month
    #         domain.append(('month', '=', month))
    #         print domain
    #         sms_teacher_per_month_id = s.env['sms.teacher.per.month'].search(domain)
    #         print sms_teacher_per_month_id
    #         s.sms_teacher_per_month_id = sms_teacher_per_month_id._ids

    # @api.multi
    # @api.depends('date_sending', 'school_id', 'teacher_id')
    # def sms_numbers_limit(self):
    #     for s in self:
    #         date_sending = s.change_utc_to_local_datetime(s.date_sending)
    #         date_sending = datetime.strptime(date_sending, '%Y-%m-%d %H:%M:%S')
    #         domain = []
    #         if s.school_id:
    #             school_id = s.school_id.id
    #             domain.append(('school_id', '=', school_id))
    #         if s.teacher_id:
    #             teacher_id = s.teacher_id.id
    #             domain.append(('teacher_id', '=', teacher_id))
    #         years = date_sending.year
    #         domain.append(('years', '=', years))
    #         months = date_sending.month
    #         domain.append(('months', '=', months))
    #         s.sms_numbers_limit = s.env['sms.teacher.per.month'].search(domain).id

    @api.onchange('is_send_all')
    def _send_all_change(self):
        if self.is_send_all and self.study_class_id:
            domain = [('study_class_id', '=', self.study_class_id.id)]
            student_ids = self.env['bms.student.management'].search(domain)._ids
            self.student_ids = [(6, False, student_ids)]
        if not self.is_send_all:
            self.student_ids = []

    @api.onchange('group_student_ids')
    def _send_group_change(self):
        if self.group_student_ids and self.study_class_id:
            student_ids = []
            for group in self.group_student_ids:
                for student in group.student_ids:
                    student_ids.append(student.id)
            self.student_ids = [(6, False, student_ids)]
            print(group.student_ids)
        if not self.group_student_ids:
            self.student_ids = []

    #xử lý việc nhập dữ liệu có dấu hoặc không dấu
    @api.multi
    @api.depends('message_no_accent', 'message_accent')
    def _compute_message(self):
        for s in self:
            if s.accent_vietnamese == 'no_accent':
                s.message = s.message_no_accent
            elif s.accent_vietnamese == 'accent':
                s.message = s.message_accent

    # @api.one
    # def _set_message(self):
    #     self.write({'message': self.message_compute})

    @api.onchange('accent_vietnamese')
    def _change_accent_vietnamese(self):
        self.message = False
        self.message_no_accent = False
        self.message_accent = False

    # Chỉnh sửa hàm này để quy ra số tn gửi cho mỗi học sinh trong lần gửi này.
    @api.multi
    @api.onchange('accent_vietnamese', 'message_no_accent', 'message_accent')
    def _compute_sms_sending_per_student(self):
        for s in self:
            if s.accent_vietnamese == 'no_accent':
                if not s.message_no_accent:
                    s.sms_sending_per_student = 0
                    continue
                if len(s.message_no_accent) <= 160:
                    s.sms_sending_per_student = 1
                elif len(s.message_no_accent) <= 306:
                    s.sms_sending_per_student = 2
                elif len(s.message_no_accent) <= 459:
                    s.sms_sending_per_student = 3
                elif len(s.message_no_accent) <= 612:
                    s.sms_sending_per_student = 4
                elif len(s.message_no_accent) <= 765:
                    s.sms_sending_per_student = 5
                else:
                    raise UserError(_('Nội dung tin nhắn vượt quá giới hạn, '
                                      'chỉnh sửa lại nội dung cho phù hợp'))
            elif s.accent_vietnamese == 'accent':
                if not s.message_accent:
                    s.sms_sending_per_student = 0
                    continue
                if len(s.message_accent) <= 70:
                    s.sms_sending_per_student = 1
                elif len(s.message_accent) <= 134:
                    s.sms_sending_per_student = 2
                elif len(s.message_accent) <= 201:
                    s.sms_sending_per_student = 3
                elif len(s.message_accent) <= 268:
                    s.sms_sending_per_student = 4
                elif len(s.message_accent) <= 335:
                    s.sms_sending_per_student = 5
                else:
                    raise UserError(_('Nội dung tin nhắn vượt quá giới hạn, '
                                      'chỉnh sửa lại nội dung cho phù hợp'))
            else:
                pass

    @api.onchange('student_ids')
    @api.depends('sms_sending_per_student')
    def _compute_sms_total(self):
        if self.student_ids:
            self.sms_sending_total_number = self.sms_sending_per_student * len(
                self.student_ids)
        else:
            self.sms_sending_total_number = 0

    # @api.multi
    # def action_send(self):
    #     for s in self:
    #         if s.state == 'draft':
    #             s.write({'state': 'processing'})
    #         for student in s.student_ids:
    #             mobile = student.parent_id.mobile
    #             val = {
    #                 'to_number': mobile,
    #                 'message': s.message,
    #                 'sms_management_id': s.id,
    #             }
    #             message = self.env['bms.sms.compose'].create(val)
    #             message.send_entity()
    #
    #     return True

    def _check_mobile(self, mobile):
        if mobile:
            mobile_prefix = '84'
            if mobile.startswith("0"):
                mobile = mobile_prefix + mobile[1:].replace(" ", "")
            elif mobile.startswith("+"):
                mobile = mobile.replace("+", "")
            else:
                mobile = mobile.replace(" ", "")
        return mobile

    @api.multi
    def action_send(self):
        # print("---------------test truc tiep--------------")
        # tatata = self.env['sms.management'].create({
        #     'name': 'Ha hah ha',
        #     'school_id': 3,
        #     'sehoot_year_id': 1,
        #     'study_class_id': 1,
        #     'date_sending': '2018-03-17 12:38:24',
        #     'message_no_accent': 'asdasdas asdasdasd',
        #     # 'group_student_ids': (6,0,[1,2]),
        #     # 'group_student_ids': (6,0,{1,2}),
        #     'group_student_ids': (1, 2),
        #
        #     'is_send_all': False,
        # })
        # print(tatata)
        print("-----da gui--------------")
        for s in self:
            if s.sms_remain >= s.sms_sending_total_number:
                if s.state == 'draft':
                    s.write({'state': 'processing'})
                mobiles = []
                for student in s.student_ids:
                    if student.parent_id.mobile:
                        mobile = s._check_mobile(student.parent_id.mobile)
                        mobiles.append(mobile)
                mobiless = ",".join((mobile) for mobile in mobiles)
                val = {
                    'to_number': mobiless,
                    'message': s.message,
                    'sms_management_id': s.id,
                }
                message = self.env['bms.sms.compose'].sudo().create(val)
                send = message.send_entity()
                if send:
                    s.write({'state': 'done'})
                else:
                    s.write({'state': 'error'})
            else:
                raise UserError(_('Số tin nhắn có thể gửi không đủ để thực hiện lần gửi tin nhắn này'))
        # print("*"*30)
        self._compute_sms_remain()
        this_teacherid = self.teacher_id.id
        # print(this_teacherid)
        sl_conlai = self.sms_remain
        # print(sl_conlai)
        betax = self.env['hr.employee'].sudo().browse(this_teacherid).write({'sms_remain': sl_conlai})
        # print("*"*30)
        return True

    # @api.model
    # def action_send_by_queue(self):
    #     domain = [('state', '=', 'draft')]
    #     limit = self.env['ir.config_parameter'].get_param('sms_limit_queue')
    #     try:
    #         limit = int(limit)
    #     except:
    #         limit = 30
    #     sms = self.search(domain, limit=limit, order='create_date')
    #     if not sms:
    #         return True
    #     for s in sms:
    #         s.send_entity()
    #     return True
    #
    # @api.model
    # def action_send_by_queue_error(self):
    #     domain = [('state', '=', 'error')]
    #     limit = self.env['ir.config_parameter'].get_param('sms_limit_queue')
    #     try:
    #         limit = int(limit)
    #     except:
    #         limit = 30
    #     sms = self.search(domain, limit=limit, order='create_date')
    #     if not sms:
    #         return True
    #     for s in sms:
    #         s.send_entity()
    #     return True

    @api.multi
    def action_resend(self):

        for s in self:
            # if s.state == 'error':
            #     s.write({'state': 'processing'})
            s.action_send()
        return True

    @api.multi
    def action_done(self):
        for s in self:
            if s.state == 'processing':
                s.write({'state': 'done'})
        return True

    @api.multi
    def action_cancel(self):
        for s in self:
            if s.state == 'draft':
                s.write({'state': 'cancel'})
            elif s.state == 'error':
                s.write({'state': 'cancel'})
        return True

    @api.multi
    @api.depends('teacher_id')
    def _compute_sms_remain(self):
        date_now = datetime.now()
        from_date = date_now.strftime('%Y-%m') + '-01'
        from_date = from_date + ' 00:00:00'
        from_date = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S')
        from_date = datetime.strftime(from_date, '%Y-%m-%d %H:%M:%S')
        print from_date
        date_now = datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        to_date = date_now.strftime('%Y-%m-%d')
        to_date = to_date + ' 23:59:59'
        to_date = datetime.strptime(to_date, '%Y-%m-%d %H:%M:%S')
        to_date = datetime.strftime(to_date, '%Y-%m-%d %H:%M:%S')
        domain = [('teacher_id', '=', self.teacher_id.id),
                  ('date', '>=', from_date),
                  ('date', '<=', to_date),
                  ('status', '=', _('send_success'))]
        for s in self:
            total_sms_sended = s.env['bms.sms.log'].sudo().search_count(domain)
            if s.sms_numbers_limit == -1:
                raise UserError(_('Giáo viên không được gửi tin nhắn'))
            elif s.sms_numbers_limit == 0:
                s.sms_remain = 10000
            else:
                s.sms_remain = s.sms_numbers_limit - total_sms_sended
                sl_conlai = s.sms_remain
                print(sl_conlai)
                this_teacherid = self.teacher_id
                print(this_teacherid)
                # self.env['hr.employee'].browse(this_teacherid).write({'sms_remain':sl_conlai})
