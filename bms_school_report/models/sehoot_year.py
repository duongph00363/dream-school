# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class BMSSehootYear(models.Model):
    _name = 'bms.sehoot.year'
    _description = 'Bms Sehoot Year'

    name = fields.Char(compute='_compute_name_sehoot_year', store=True)
    # name = fields.Char()
    school_id = fields.Many2one('res.company')
    from_date = fields.Date(required=True, default=lambda self: fields.Date.today())
    to_date = fields.Date(required=True)
    study_class_ids = fields.One2many('bms.study.class', 'sehoot_year_id')
    student_history_ids = fields.One2many('bms.student.history',
                                          'sehoot_year_old_id')
    active = fields.Boolean(default=True)

    @api.multi
    @api.depends('from_date', 'to_date')
    def _compute_name_sehoot_year(self):
        for s in self:
            from_date = s.from_date
            to_date = s.to_date
            year_from_date_temp = unicode(from_date).split('-')
            if len(year_from_date_temp):
                year_from_date = year_from_date_temp[0]
            year_to_date_temp = unicode(to_date).split('-')
            if len(year_from_date_temp):
                year_to_date = year_to_date_temp[0]
            s.name = str(year_from_date) + _('-') + str(year_to_date)

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        try:
            # self.action_confirm()
            list_year = self.env['bms.sehoot.year'].search([('active', '=', True), ('school_id', '=', vals['school_id'])])
            if len(list_year) > 0:
                for year in list_year:
                    year.write({'active': False})
            res = super(BMSSehootYear, self).create(vals)
            return res
        except Exception as e:
            _logger.error(e)
            
    @api.multi
    def active_year(self):
        if self.active:
            self.active = False
        else:
            list_active_record = self.env['bms.sehoot.year'].search([('school_id', '=', self.school_id.id), ('active', '=', True)])
            if list_active_record:
                for active_record in list_active_record:
                    active_record.write({'active' : False})
            self.active = True
