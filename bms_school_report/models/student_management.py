# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import fields, models, api


class BMSStudentManagement(models.Model):
    _name = 'bms.student.management'
    _description = 'Bms Student Management'

    name = fields.Char(required=True)
    student_code = fields.Char()
    street = fields.Char()
    country_id = fields.Many2one('res.country', string='Country',
                                 required=True,
                                 default=lambda self: self.env.ref('base.vn'))
    state_id = fields.Many2one('res.country.state',
                               string='State',
                               domain=lambda self:
                               [('country_id', '=',
                                 self.env.ref('base.vn').id)])
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    gender = fields.Selection([('male', 'Nam'),
                               ('female', 'Nữ'),
                               ('undefine', 'Không xác định')],
                              default='male')
    birth_day = fields.Date()
    nation = fields.Char()
    age = fields.Integer(compute='_compute_age')
    identification_number = fields.Char()
    date_of_issue = fields.Date()
    place_of_issue = fields.Char()
    study_class_id = fields.Many2one('bms.study.class', string='Lớp')
    group_of_class_id = fields.Many2one('bms.group.of.class', compute='_compute_group_of_class_id',  store=True)
    # school_id = fields.Many2one('res.company', compute='_compute_school_id',
    #                             store=True)
    school_id = fields.Many2one('res.company', store=True)
    sehoot_year_id = fields.Many2one('bms.sehoot.year')
    note = fields.Text()
    phone = fields.Char()
    email = fields.Char()
    parent_id = fields.Many2one('res.partner', string='Phụ huynh')
    student_history_ids = fields.One2many('bms.student.history', 'student_id')
    user_id = fields.Many2one('res.users', string="Tài khoản đăng kí",
                              readonly=True, copy=False)
    user_id_email = fields.Char(string='Thông tin đăng nhập', store=True, related='user_id.email')

    group_student_ids = fields.Many2many('group.student',
                                         'group_student_student_management_rel',
                                         'student_id', 'group_student_id',
                                         string='Nhóm học sinh')

    tin_nhan_ids = fields.Many2many('sms.management')

    @api.multi
    @api.depends('study_class_id')
    def _compute_group_of_class_id(self):
        for student in self:
            student.group_of_class_id = student.study_class_id.group_of_class_id

    # @api.multi
    # @api.depends('study_class_id')
    # def _compute_school_id(self):
    #     for student in self:
    #         student.school_id = student.study_class_id.group_of_class_id.school_id

    @api.multi
    @api.depends('birth_day')
    def _compute_age(self):
        for student in self:
            year_today = datetime.utcnow().year
            if self.birth_day:
                year_birth_day = (datetime.strptime(student.birth_day, '%Y-%m-%d')).year
                self.age = year_today - year_birth_day

    @api.multi
    def change_class(self):
        action = self.env.ref(
            'bms_school_report.action_bms_student_history_form')
        action_view = action.read()
        action_view[0]['context'] = {
            'default_student_id': self.id,
            'default_study_class_old_id': self.study_class_id.id,
            'default_sehoot_year_old_id': self.sehoot_year_id.id,
        }
        return action_view[0]

    _sql_constraints = [
        ('student_code_uniq', 'unique(student_code)',
         'Mã học sinh phải là duy nhất!')
    ]

    @api.multi
    def create_user(self):
        user_value = {
            'name': self.name,
            'login': self.student_code,
            'password': '123456',
            'company_ids': [(6, False, [self.school_id.id])],
            'company_id': self.school_id.id,
            'groups_id': [(6, False, [self.env.ref(
                'bms_school_report.group_user_parent').id])],
            # 'partner_id': self.partner_id.id
        }
        user_id = self.env['res.users'].sudo().create(user_value)
        self.write({'user_id': user_id.id})
        return user_id

    @api.onchange('school_id')
    def change_school(self):
        if self.school_id:
            self.sehoot_year_id = []
            self.study_class_id = []
    @api.onchange('sehoot_year_id')
    def change_sehoot(self):
        if self.sehoot_year_id:
            self.study_class_id = []
