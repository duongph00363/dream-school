# -*- coding: utf-8 -*-
from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def _default_school_id(self):
        school_id = self.env.user.company_id
        return school_id

    identity_card = fields.Char()
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Không xác định')
    ], string='Giới tính')
    birthday = fields.Date()
    note = fields.Text()
    children_ids = fields.One2many('bms.student.management', 'parent_id',
                                   readonly=True)
    # user_id = fields.Many2one('res.users', 'User Registry', readonly=True,
    #                           copy=False)
    # user_id = fields.Many2one('res.users', string="Tài khoản đăng kí", readonly=True, copy=False)
    # is_parent = fields.Boolean(compute='_compute_is_parent', store=True)
    school_id = fields.Many2one('res.company', default=_default_school_id)
    is_parent = fields.Boolean(store=True)
    user_ids = fields.Many2many('res.users', 'bms_student_management',
                                'parent_id', 'user_id', readonly=True, string=u'Các tài khoản')

    # @api.depends('user_ids.groups_id')
    # @api.multi
    # def _compute_is_parent(self):
    #     for p in self:
    #         p.is_parent = False
    #         for user in p.user_ids:
    #             if user.has_group('bms_school_report.group_user_parent'):
    #                 p.is_parent = True
    #                 break

    # xu ly compute school_id
    # @api.multi
    # @api.depends('name')
    # def _compute_school_id(self):
    #     for p in self:
    #             p.school_id = self.env.user.company_id.id
    #             print p.school_id

    # @api.multi
    # def create_user(self):
    #     user_value = {
    #         'name': self.name,
    #         'login': self.email,
    #         'password': '123456',
    #         'company_ids': [(6, False, [self.school_id.id])],
    #         'company_id': self.school_id.id,
    #         'groups_id': [(6, False, [self.env.ref(
    #             'bms_school_report.group_user_parent').id])],
    #         'partner_id': self.id
    #     }
    #     user_id = self.env['res.users'].create(user_value)
    #     self.write({'user_id': user_id.id})
    #     return user_id
    #
    # @api.model
    # def default_get(self, fields):
    #     res = super(ResPartner, self).default_get(fields)
    #     print res
    #     return res
    #
    # @api.model
    # def create(self, vals):
    #     vals['school_id'] = self.env.user.company_id.id
    #     print vals
    #     return super(ResPartner, self).create(vals)

    # @api.onchange('email')
    # def onchange_email(self):
    #     return False
    #
    # @api.onchange('parent_id')
    # def onchange_parent_id(self):
    #     return False
    #
    # @api.onchange('company_type')
    # def onchange_company_type(self):
    #     return False
    #
    # @api.onchange('country_id')
    # def _onchange_country_id(self):
    #     return False