# -*- coding: utf-8 -*-
# Copyright (C) 2006 BMS Group Global (<http://www.bmsgroupglobal.com>).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


import logging
import re
import urllib
from datetime import datetime

import werkzeug.exceptions

from odoo import api, exceptions, fields, models
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)
p = '^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$'

try:
    import requests
except ImportError:
    _logger.warn('Cannot `import requests`.')


class SMSCompose(models.TransientModel):
    _name = "bms.sms.compose"

    to_number = fields.Char()
    message = fields.Char()
    sms_management_id = fields.Many2one('sms.management')
    state = fields.Selection([('draft', 'Mới'),
                              ('sending', 'Đang gửi'),
                              ('error', 'Lỗi'),
                              ('done', 'Hoàn thành'),
                              ('cancel', 'Hủy bỏ')], default='draft')
    principal_management_id = fields.Many2one('principal.sms')

    nguoi_nhan_ids = fields.Many2many('res.users')

    # @api.multi
    # @api.constrains('to_number')
    # def _check_phone_number(self):
    #     for rec in self:
    #         if rec.to_number and not re.match(p, rec.to_number):
    #             raise exceptions.ValidationError(_(
    #                 'The mobile number is not available'))
    #     return True

    # @api.multi
    # @api.constrains('message')
    # def _check_message_ascii(self):
    #     for rec in self:
    #         is_ascii = all(ord(char) < 128 for char in rec.message)
    #         if not is_ascii:
    #             raise exceptions.ValidationError(_(
    #                 'The content of message is invalid'))
    #     return True

    @api.multi
    # @api.one
    def send_entity(self):
        config = self.env['bms.sms.config'].search([])
        # sms_log = self.env['bms.sms.log']
        url_accent_config = config.url_accent
        url_no_accent_config = config.url_no_accent
        url_config_dict = {
            'no_accent': url_no_accent_config,
            'accent': url_accent_config,
        }

        url_config = url_config_dict.get(self.sms_management_id.accent_vietnamese)
        if url_config:
            values = {'username': config.username,
                      'password': config.password,
                      'source_addr': config.brand_name,
                      'dest_addr': self.to_number,
                      'message': self.message}
            ary_ordered_names = []
            ary_ordered_names.append('username')
            ary_ordered_names.append('password')
            ary_ordered_names.append('source_addr')
            ary_ordered_names.append('dest_addr')
            ary_ordered_names.append('message')
            # getdata = "&".join(
            #     [item + '=' + urllib.quote_plus(values[item]) for item in
            #      ary_ordered_names])
            getdata = "&".join([item + '=' + (values[item]) for item in ary_ordered_names])

            param_no_accent = url_config + '?' + getdata
            param_accent = url_config + '?' + getdata + '&type=1&request_id=1&telco_code=1'
            param_dict = {
                'no_accent': param_no_accent,
                'accent': param_accent,
            }
            # print param_dict.get(self.sms_management_id.accent_vietnamese)
            if requests:
                try:
                    response_string = requests.get(param_dict.get(self.sms_management_id.accent_vietnamese))
                    if response_string:
                        print response_string
                        try:
                            code = response_string.text.encode('utf-8')
                            code_eval = eval(code)
                            for item_code in code_eval:
                                to_number = str(item_code['dest_addr'])
                                code = str(item_code['status'])
                                sms_log = self.env['bms.sms.log'].create({
                                    'name': self.message,
                                    'to_number': to_number,
                                    'date': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                    'code': code,
                                    'sms_management_id': self.sms_management_id.id,
                                    'teacher_id': self.sms_management_id.teacher_id.id,
                                })
                        except:
                            print 'error conver log'
                            return False
                except requests.exceptions.RequestException as exception:
                    if exception:
                        return False
        return True

    # def _make_error_response(self, status, message):
    #     exception = werkzeug.exceptions.HTTPException()
    #     print exception
    #     exception.code = status
    #     exception.description = message
    #     return exception
    @api.multi
    # @api.one
    def send_entity_teacher(self):
        config = self.env['bms.sms.config'].search([])
        # sms_log = self.env['bms.sms.log']
        url_accent_config = config.url_accent
        url_no_accent_config = config.url_no_accent
        url_config_dict = {
            'no_accent': url_no_accent_config,
            'accent': url_accent_config,
        }
        url_config = url_config_dict.get(self.principal_management_id.accent_vietnamese)
        if url_config:
            values = {'username': config.username,
                      'password': config.password,
                      'source_addr': config.brand_name,
                      'dest_addr': self.to_number,
                      'message': self.message}
            ary_ordered_names = []
            ary_ordered_names.append('username')
            ary_ordered_names.append('password')
            ary_ordered_names.append('source_addr')
            ary_ordered_names.append('dest_addr')
            ary_ordered_names.append('message')
            # getdata = "&".join(
            #     [item + '=' + urllib.quote_plus(values[item]) for item in
            #      ary_ordered_names])
            getdata = "&".join([item + '=' + (values[item]) for item in ary_ordered_names])

            param_no_accent = url_config + '?' + getdata
            param_accent = url_config + '?' + getdata + '&type=1&request_id=1&telco_code=1'
            param_dict = {
                'no_accent': param_no_accent,
                'accent': param_accent,
            }
            print("-----------param_dict---------")
            print(param_dict)
            if requests:
                try:
                    response_string = requests.get(param_dict.get(self.principal_management_id.accent_vietnamese))
                    if response_string:
                        print response_string
                        try:
                            code = response_string.text.encode('utf-8')
                            code_eval = eval(code)
                            for item_code in code_eval:
                                to_number = str(item_code['dest_addr'])
                                code = str(item_code['status'])
                                sms_log = self.env['bms.sms.log'].create({
                                    'name': self.message,
                                    'date': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                    'code': code,
                                    'principal_management_id': self.principal_management_id.id,
                                    'sms_management_id': self.sms_management_id.id,
                                })
                        except:
                            print 'error conver log'
                            return False
                except requests.exceptions.RequestException as exception:
                    if exception:
                        return False
        return True
