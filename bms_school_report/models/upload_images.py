# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime

class UploadImages(models.Model):
    _name = 'upload.images'
    _description = 'Upload Images'

    name = fields.Char()
    study_class_id = fields.Many2one('bms.study.class')
    date = fields.Datetime('Date current action', required=False, readonly=False, select=True
                                  , default=lambda self: fields.datetime.now())
    attachment_id = fields.Many2one('ir.attachment', string='Ảnh')
    imageURL = fields.Char()
    is_sent_all_school = fields.Boolean("Gửi ảnh toàn trường")
    school = fields.Many2one("res.company", string="Trường Học")
    @api.onchange('attachment_id')
    def create_url(self):
        if self.attachment_id:
            attachment_id = self.attachment_id.id
            self.env['ir.attachment'].browse(attachment_id).write({'public': True})
            if attachment_id:
                self.imageURL = 'http://dreamschool.edu.vn/web/content/'+str(attachment_id)

    @api.onchange("study_class_id")
    def onchange_class(self):
        if (len(self.study_class_id) == 0):
            self.is_sent_all_school = True
        else:
            self.is_sent_all_school = False
            self.school = self.study_class_id.school_id

    @api.onchange("is_sent_all_school")
    def onchange_is_sent_all_school(self):
        if (self.is_sent_all_school == True):
            self.study_class_id = None
