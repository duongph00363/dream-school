# -*- coding: utf-8 -*-
from odoo import fields, models


class BmsGroupOfClass(models.Model):
    _name = 'bms.group.of.class'
    _description = 'Group Of Class'

    name = fields.Char(required=True)
    school_id = fields.Many2one('res.company', required=True)
    note = fields.Text()
    active = fields.Boolean(default=True)
    list_of_classes = fields.One2many('bms.study.class', 'group_of_class_id')

