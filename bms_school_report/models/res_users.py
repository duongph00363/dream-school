# -*- coding: utf-8 -*-
from odoo import fields, models, api


# class ResPartner(models.Model):
class ResUsers(models.Model):
    _inherit = 'res.users'

    is_teacher = fields.Boolean(compute='_compute_is_teacher', store=True)
    is_management = fields.Boolean(compute='_compute_is_management', store=True)



    @api.multi
    def _compute_is_teacher(self):
        for user in self:
            is_teacher_role = user.has_group('bms_school_report.group_user_teacher')
            is_management_role = user.has_group('bms_school_report.group_management_user')
            if is_teacher_role and not is_management_role:
                user.is_teacher = True
            else:
                user.is_teacher = False

    @api.multi
    def _compute_is_user_management(self):
        for user in self:
            is_management_role = user.has_group(
                'bms_school_report.group_management_user')
            if is_management_role:
                user.is_management = True
            else:
                user.is_management = False
