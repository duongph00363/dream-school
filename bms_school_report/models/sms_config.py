# -*- coding: utf-8 -*-
from odoo import models, fields


class SMSConfig(models.Model):
    _name = 'bms.sms.config'
    _description = 'SMS Config'

    username = fields.Char(string='UserName')
    password = fields.Char()
    brand_name = fields.Char()
    url_no_accent = fields.Char(string='URL No Accent')
    url_accent = fields.Char(string='URL Accent')
