# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class BMSStudyClass(models.Model):
    _name = 'bms.study.class'
    _description = 'Bms Study Class'

    def _school_id_default(self):
        is_management = self.env.user.has_group(
            'bms_school_report.group_management_user')
        if is_management:
            school_id = self.env.user.company_id
            return school_id and school_id[0] or False

    name = fields.Char(required=True)
    sehoot_year_id = fields.Many2one('bms.sehoot.year', required=True)
    homeroom_teacher = fields.Many2one('hr.employee')
    student_numbers = fields.Integer(compute='_compute_student_numbers', store=True)
    class_room_number = fields.Integer()
    group_of_class_id = fields.Many2one('bms.group.of.class', required=True)
    school_id = fields.Many2one('res.company', default=_school_id_default, store=True)
    student = fields.Char()
    note = fields.Text()
    teacher_ids = fields.Many2many('hr.employee',
                                   'teacher_management_study_class_rel',
                                   'study_class_id',
                                   'teacher_id')
    student_ids = fields.One2many('bms.student.management', 'study_class_id')
    student_history_ids = fields.One2many('bms.student.history', 'study_class_old_id')
    user_ids = fields.Many2many('res.users', 'bms_student_management', 'study_class_id', 'user_id', readonly=True, string=u'Các tài khoản')
    active = fields.Boolean(default=True)
    upload_images_ids = fields.One2many('upload.images', 'study_class_id')

    @api.multi
    @api.depends('student_ids')
    def _compute_student_numbers(self):
        for cl in self:
            cl.student_numbers = len(cl.student_ids)

    # @api.multi
    # @api.depends('group_of_class_id')
    # def _compute_school_id(self):
    #     for cl in self:
    #         # cl.school_id = cl.group_of_class_id.school_id
    #         cl.school_id = cl.env.user.company_id.id
    # @api.model
    # @api.returns('self', lambda value: value.id)
    # def create(self, vals):
    #     try:
    #         if 'homeroom_teacher' in vals:
    #             homeroom_exists = self.env['bms.study.class'].search([('homeroom_teacher', '=', vals['homeroom_teacher'])])
    #             if len(homeroom_exists) > 0:
    #                 vals['homeroom_teacher'] = False
    #                 res = super(BMSStudyClass, self).create(vals)
    #                 return res                        
    #         res = super(BMSStudyClass, self).create(vals)
    #         return res
    #     except Exception as e:
    #         _logger.error(e)

    # def write(self, vals):
    #     try:
    #         if 'homeroom_teacher' in vals:
    #             homeroom_exists = self.env['bms.study.class'].search([('homeroom_teacher', '=', vals['homeroom_teacher'])])
    #             if len(homeroom_exists) > 0:
    #                 vals['homeroom_teacher'] = False
    #                 res = super(BMSStudyClass,self).write(vals)
    #                 _logger.info("Edited a new record successful")
    #                 return res
    #         res = super(BMSStudyClass,self).write(vals)
    #         _logger.info("Edited a new record successful")
    #         return res               
    #     except ValueError as er:
    #         _logger.error(er)
            
    # @api.onchange('homeroom_teacher')
    # def homeroom_teacher_contrains(self):
    #     if self.homeroom_teacher.id:
    #         homeroom_exists = self.env['bms.study.class'].search([('homeroom_teacher', '=', self.homeroom_teacher.id)])
    #         if len(homeroom_exists) > 0:
    #             self.homeroom_teacher = []
    #             raise UserError(_(
    #                     'Cảnh báo: '
    #                     'Giáo viên đã chủ nhiệm đã được chọn cho lớp khác'))