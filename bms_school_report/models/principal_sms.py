# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo import fields, models, api, _
import pytz
from odoo.exceptions import UserError


class PrincipalSMS(models.Model):
    _name = 'principal.sms'
    _description = 'Principal SMS'
    name = fields.Char()
    school_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, store=True, required=True)
    is_send_all_teachers = fields.Boolean()
    is_send_all_students = fields.Boolean()
    is_send_all_school = fields.Boolean()
    teacher_ids = fields.Many2many('hr.employee', 'teacher_id_many_rel', 'principal_id', 'hr_employee_id',
                                   string='Giáo viên')
    parent_ids = fields.Many2many('res.partner',
                                   string='Phụ huynh')
    student_ids = fields.Many2many('bms.student.management',
                                   'sms_management_student_management_rel',
                                   'sms_management_id', 'student_id',
                                   string='Học sinh')
    date_sending = fields.Datetime(required=True, datedefault=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    state = fields.Selection([('draft', 'Soạn SMS'),
                              ('processing', 'Đang gửi tin'),
                              ('error', 'Lỗi'),
                              ('done', 'Hoàn thành'),
                              ('cancel', 'Hủy bỏ')],
                             required=True, readonly=True,
                             default='draft')
    accent_vietnamese = fields.Selection([('no_accent', 'Tiếng Việt không dấu'),
                                          ('accent', 'Tiếng Việt có dấu')], default='no_accent')
    message = fields.Text(compute='_compute_message', store=True)
    message_no_accent = fields.Text()
    message_accent = fields.Text()
    principle_sms_log_ids = fields.One2many('bms.sms.log', 'principle_sms_management_id', string='SMS Log')

    @api.multi
    def change_utc_to_local_datetime(self, source_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.strptime(source_date, '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    @api.onchange('is_send_all_teachers')
    def _send_all_teachers_change(self):
        if self.is_send_all_teachers:
            domain = [('school_id', '=', self.school_id.id)]
            teacher_id = self.env['hr.employee'].search(domain).ids
            if teacher_id:
                self.teacher_ids = [(6, False, teacher_id)]
        else:
            self.teacher_ids = []

    @api.onchange('is_send_all_students')
    def _send_all_students_change(self):
        if self.is_send_all_students:
            domain = [('school_id', '=', self.school_id.id), ('is_parent', '=', True)]
            parent_ids = self.env['res.partner'].search(domain).ids
            if parent_ids:
                self.parent_ids = [(6, False, parent_ids)]
        else:
            self.parent_ids = []

    @api.onchange('is_send_all_school')
    def _send_all_school_change(self):
        if self.is_send_all_school:
            domain = [('school_id', '=', self.school_id.id)]
            teacher_id = self.env['hr.employee'].search(domain).ids
            if teacher_id:
                # teacher_id = map(lambda x: int(x), teacher_id)
                self.teacher_ids = [(6, False, teacher_id)]

            parent_id = self.env['res.partner'].search(domain).ids
            if parent_id:
                # teacher_id = map(lambda x: int(x), teacher_id)
                self.parent_ids = [(6, False, parent_id)]
        else:
            self.teacher_ids = []
            self.parent_ids = []

    @api.multi
    # @api.model
    @api.depends('message_no_accent', 'message_accent')
    def _compute_message(self):
        for s in self:
            if s.accent_vietnamese == 'no_accent':
                s.message = s.message_no_accent
            elif s.accent_vietnamese == 'accent':
                s.message = s.message_accent

    @api.multi
    def _check_mobile(self, mobile):
        check_phone(mobile)

    @api.multi
    def action_send(self):
        for s in self:
            if s.state == 'draft' or s.state == 'error':
                s.write({'state': 'processing'})
                mobiles = []
                for teacher in s.teacher_ids:
                    if teacher.mobile_phone:
                        mobile = s._check_mobile(teacher.mobile_phone)
                        mobiles.append(mobile)
                    mobiless = ",".join((mobile) for mobile in mobiles)

                for parents in s.parent_ids:
                    if parents.mobile:
                        mobile = s._check_mobile(parents.mobile)
                        mobiles.append(mobile)
                    mobiless = ",".join((mobile) for mobile in mobiles)

                val = {
                    'to_number': mobiless,
                    'message': s.message,
                    'principal_management_id': s.id,
                }
                message = self.env['bms.sms.compose'].sudo().create(val)
                send = message.send_entity_teacher()
                code = 0
                if send:
                    s.write({'state': 'done'})
                    code = 1
                else:
                    s.write({'state': 'error'})
                    code = -1

                log = {
                    'name': s.message,
                    'to_number': mobiles,
                    'date': s.date_sending,
                    'code': code,
                    'status': s.state,
                    'teacher_id': self.env.user.id,
                    'principle_sms_management_id': s.id

                }
                self.env['bms.sms.log'].sudo().create(log)
            else:
                raise UserError(_('Lỗi không thể gửi tin nhắn!.'))

        return True

    @api.onchange('accent_vietnamese')
    def _change_accent_vietnamese(self):
        self.message = False
        self.message_no_accent = False
        self.message_accent = False

    @api.multi
    def action_resend(self):
        for s in self:
            # if s.state == 'error':
            #     s.write({'state': 'processing'})
            s.action_send()
        return True

    @api.multi
    def action_done(self):
        for s in self:
            if s.state == 'processing':
                s.write({'state': 'done'})
        return True

    @api.multi
    def action_cancel(self):
        for s in self:
            if s.state == 'draft':
                s.write({'state': 'cancel'})
            elif s.state == 'error':
                s.write({'state': 'cancel'})
        return True


def check_phone(mobile):
    if mobile:
        mobile_prefix = '84'
        if mobile.startswith("0"):
            mobile = mobile_prefix + mobile[1:].replace(" ", "")
        elif mobile.startswith("+"):
            mobile = mobile.replace("+", "")
        else:
            mobile = mobile.replace(" ", "")
    return mobile