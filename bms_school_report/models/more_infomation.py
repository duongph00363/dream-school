# -*- coding: utf-8 -*-
from odoo import fields, models, api, _

class MoreInfomation(models.Model):
    _inherit = 'res.config.settings'
    _name = 'more.infomation'
    _description = 'More infomation'

    website = fields.Char(string='Website')
    facebook = fields.Char(string='Facebook')
    email = fields.Char(string='Email')
    phone = fields.Char(string='Số điện thoại')
    address = fields.Char(string='Địa chỉ')
    intro = fields.Text(string='Giới thiệu')