# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class SMSLog(models.Model):
    _name = 'bms.sms.log'
    _rec_name = 'id'

    name = fields.Char(string='Message')
    to_number = fields.Char()
    date = fields.Datetime()
    code = fields.Char()
    sms_management_id = fields.Many2one('sms.management', ondelete='cascade')
    status = fields.Char(compute='_compute_status_sms', store=True)
    teacher_id = fields.Many2one('hr.employee')
    principle_sms_management_id = fields.Many2one('principal.sms', ondelete='cascade')

    @api.multi
    @api.depends('code')
    def _compute_status_sms(self):
        for s in self:
            if int(s.code) < 0:
                s.status = _('send_error')
            else:
                s.status = _('send_success')
