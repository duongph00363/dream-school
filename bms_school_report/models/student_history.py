# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import fields, models, api


class BMSStudentHistory(models.Model):
    _name = 'bms.student.history'
    _description = 'Bms Student History'

    # def _default_current_student(self):
    #     if not self.student_id:
    #         return self.env.student_id.id
    #     return False

    student_id = fields.Many2one('bms.student.management', readonly=True)
    school_id = fields.Many2one('res.company', compute='_compute_school_id', store=True)
    study_class_old_id = fields.Many2one('bms.study.class')
    sehoot_year_old_id = fields.Many2one('bms.sehoot.year')
    # study_class_id = fields.Many2one('bms.study.class',
    #                                  compute='_compute_study_class_id')
    # sehoot_year_id = fields.Many2one('bms.sehoot.year',
    #                                  compute='_compute_sehoot_year_id')
    study_class_new_id = fields.Many2one('bms.study.class')
    sehoot_year_new_id = fields.Many2one('bms.sehoot.year')

    @api.multi
    @api.depends('student_id')
    def _compute_school_id(self):
        for student in self:
            student.school_id = student.study_class_old_id.group_of_class_id.school_id

    @api.multi
    def save(self):
        vals = {
            'sehoot_year_id': self.sehoot_year_new_id.id,
            'study_class_id': self.study_class_new_id.id,
        }
        res = self[0].student_id.write(vals)
        return res
