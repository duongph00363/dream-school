# -*- coding: utf-8 -*-
from odoo import fields, models, api


class GroupStudent(models.Model):
    _name = 'group.student'
    _description = 'Group Student'

    name = fields.Char()
    school_id = fields.Many2one('res.company', store=True, required=True)
    sehoot_year_id = fields.Many2one('bms.sehoot.year', required=True)
    study_class_id = fields.Many2one('bms.study.class', string='Lớp', required=True)
    active = fields.Boolean(default=True, store=True)
    student_ids = fields.Many2many('bms.student.management',
                                   'group_student_student_management_rel',
                                   'group_student_id', 'student_id')

    student_numbers = fields.Integer(compute='_compute_student_numbers',
                                    readonly=True,
                                    store=True)

    @api.multi
    @api.depends('student_ids')
    def _compute_student_numbers(self):
        for gr in self:
            gr.student_numbers = len(gr.student_ids)