# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.exceptions import UserError
from odoo.tools.translate import _


class BMSIdeaBox(models.Model):
    _name = 'bms.idea.box'
    _inherit = ['mail.thread']
    _description = 'Ý kiến phụ huynh'
    _rec_name = 'id'

    parent_id = fields.Many2one('res.partner', string='Phụ huynh',
                                 required=True)
    content = fields.Text('Idea content', required=True, readonly=True,
                          states={'draft': [('readonly', False)]})

    state = fields.Selection([('draft', 'Tạo mới'),
                             ('processing', 'Đang xử lý'),
                             ('done', 'Hoàn thành'),
                             ('cancel', 'Hủy bỏ')],
                             required=True, readonly=True,
                             default='draft')

    day_create = fields.Date(default=lambda self: fields.Date.today())

    @api.multi
    def action_processing(self):
        for idea in self:
            if idea.state == 'draft':
                idea.write({'state': 'processing'})
        return True

    @api.multi
    def action_done(self):
        for idea in self:
            if idea.state == 'processing':
                idea.write({'state': 'done'})
        return True

    @api.multi
    def action_cancel(self):
        parent_flag = self.env.user.has_group('bms_school_report.group_user_parent')
        teacher_flag = self.env.user.has_group('bms_school_report.group_user_teacher')
        management_flag = self.env.user.has_group('bms_school_report.group_management_user')
        for idea in self:
            if idea.state == 'draft' and parent_flag:
                idea.write({'state': 'cancel'})
            elif idea.state in ('draft', 'processing') and teacher_flag:
                idea.write({'state': 'cancel'})
            elif idea.state in ('draft', 'processing') and management_flag:
                idea.write({'state': 'cancel'})
        return True

    @api.multi
    def unlink(self):
        for idea in self:
            if idea.state != 'cancel':
                raise UserError(_(
                    'Cảnh báo: '
                    'Bạn chỉ có thể xóa ý kiến đóng góp ở trạng thái đã hủy bỏ!'))
        return super(BMSIdeaBox, self).unlink()

    def save(self):
        return True