# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import principal_sms
from random import randint

try:
    import tempfile
    import xlrd
except Exception:
    raise ValidationError(_('xlrd is required to install this module'))


class BmsSchoolManagement(models.Model):
    _inherit = 'res.company'
    _inherits = {'ir.attachment': 'file'}
    _rec_name = 'school_code'

    school_code = fields.Char()
    head_master = fields.Many2one('hr.employee')
    management_system = fields.Many2one('hr.employee')
    founded_date = fields.Date()
    district_id = fields.Many2one('res.country.district',
                                  string='Quận/huyện')
    ward_id = fields.Many2one('res.country.ward',
                              string='Phường/xã')
    school_type = fields.Selection([('cong_lap', 'Công lập'),
                                    ('ban_cong', 'Bán công'),
                                    ('dan_lap', 'Dân lập'),
                                    ('tu_thuc', 'Tư thục'),
                                    ('chuyen', 'Chuyên ban'),
                                    ('ky_thuat', 'Kỹ Thuật'),
                                    ('nang_khieu', 'Năng khiếu'),
                                    ('khac', 'Khác')],
                                   default='cong_lap', string='Loại trường')

    email = fields.Char(related='partner_id.email', store=True)

    sll_contact_type = fields.Selection([('app_only', 'Chỉ qua APP'),
                                         ('app_sms', 'Đồng thời APP & SMS'),
                                         ],
                                        default='app_sms', string='Loại sổ liên lạc')

    teachers_list = fields.Many2one('hr.employee')
    # Số tin nhắn cấp tự động cho mỗi giáo viên.
    sms_numbers_teacher = fields.Integer()
    sms_numbers_headmaster = fields.Integer()

    switchboard = fields.Char()

    # Nhập dữ liệu
    datas_student = fields.Binary()
    datas_teacher = fields.Binary()
    datas_fname_student = fields.Char()
    datas_fname_teacher = fields.Char()
    template_file = fields.Char(default=lambda self: self.env['ir.config_parameter'].get_param('web.base.url') +
                                '/bms_school_report/static/template_file/DSHS.xlsx')
    template_file_teacher = fields.Char(default=lambda self: self.env['ir.config_parameter'].get_param('web.base.url') +
                                '/bms_school_report/static/template_file/DSGV.xlsx')

    @api.multi
    def _compute_template_file(self):
        parameter = self.env['ir.config_parameter']
        base_url = parameter.get_param('web.base.url')
        url = base_url \
              + '/bms_school_report/static/template_file/DSHS.xlsx'
        for import_ds in self:
            import_ds.template_file = url

    @api.multi
    def _compute_template_file_teacher(self):
        parameter = self.env['ir.config_parameter']
        base_url = parameter.get_param('web.base.url')
        url = base_url \
              + '/bms_school_report/static/template_file/DSHS.xlsx'
        for import_ds in self:
            import_ds.template_file = url

    @api.multi
    def import_students(self):
        students = self.env['read.excel'].read_file(data=self.datas_student, sheet="Sheet1", path=False)
        if not False:
            path = '/file.xlsx'
        try:
            file_path = tempfile.gettempdir() + path
            f = open(file_path, 'wb')
            f.write(self.datas_student.decode('base64'))
            f.close()
            workbook = xlrd.open_workbook(file_path)
        except Exception:
            raise ValidationError(
                _('File format incorrect, please upload file *.xlsx format'))
        if students:
            for r in students:
                parent = self.env['res.partner'].sudo().create({'name': r[16],
                                                                'identity_card': r[17],
                                                                'gender': self.gender(r[18]),
                                                                'birthday': datetime.datetime(*xlrd.xldate_as_tuple(int(float(r[19])), workbook.datemode)),
                                                                'phone': principal_sms.check_phone(r[20]),
                                                                'mobile': principal_sms.check_phone(r[21]),
                                                                'fax': r[22],
                                                                'email': r[23],
                                                                'school_id': self.search_school(unicode(r[5]))[0],
                                                                'street': r[8],
                                                                'country_id': self.search_country(unicode(r[9]))[0],
                                                                'state_id': self.search_state(unicode(r[10]))[0],
                                                                'district_id': self.search_district(unicode(r[11]))[0],
                                                                'ward_id': self.search_ward(unicode(r[12]))[0],
                                                                'note': r[24],
                                                                'is_parent': 1
                                                                })
                if len(parent) == 0:
                    parent.ids = False
                else:
                    parent_id = self.create_parent(r[16], r[23], self.search_school(unicode(r[5]))[0], principal_sms.check_phone(r[21]))
                    parent.user_id = parent_id.id
                self.env['bms.student.management'].sudo().create({'name': r[0],
                                                                  'student_code': r[1],
                                                                  'gender': self.gender(r[2]),
                                                                  'birth_day': datetime.datetime(*xlrd.xldate_as_tuple(int(float(r[3])), workbook.datemode)),
                                                                  'nation': r[4],
                                                                  'school_id': self.search_school(unicode(r[5]))[0],
                                                                  'sehoot_year_id': self.search_sehoot(unicode(r[6]))[
                                                                      0],
                                                                  'study_class_id':
                                                                      self.search_study_class(unicode(r[7]))[0],
                                                                  'street': r[8],
                                                                  'country_id': self.search_country(unicode(r[9]))[0],
                                                                  'state_id': self.search_state(unicode(r[10]))[0],
                                                                  'district_id': self.search_district(unicode(r[11]))[
                                                                      0],
                                                                  'ward_id': self.search_ward(unicode(r[12]))[0],
                                                                  'phone': principal_sms.check_phone(r[13]),
                                                                  'email': r[14],
                                                                  'note': r[15],
                                                                  'parent_id': parent.ids[0],
                                                                  })
        else:
            raise ValidationError(u'Danh sách sinh viên trống')

    @api.multi
    def import_teachers(self):
        teachers = self.env['read.excel'].read_file(data=self.datas_teacher, sheet="Sheet1", path=False)
        if not False:
            path = '/file.xlsx'
        try:
            file_path = tempfile.gettempdir() + path
            f = open(file_path, 'wb')
            f.write(self.datas_teacher.decode('base64'))
            f.close()
            workbook = xlrd.open_workbook(file_path)
        except Exception:
            raise ValidationError(
                _('File format incorrect, please upload file *.xlsx format'))
        if teachers:
            for t in teachers:
                teacher = self.env['hr.employee'].sudo().create({'name': t[0],
                                                       'birthday': datetime.datetime(*xlrd.xldate_as_tuple(int(float(t[1])), workbook.datemode)),
                                                       'gender': self.gender(t[2]),
                                                       'marital': self.marital(t[3]),
                                                       'school_id': self.search_school(unicode(t[4]))[0],
                                                       'year_working': int(float(t[5])),
                                                       'subject': t[6],
                                                       'mobile_phone': principal_sms.check_phone(t[7]),
                                                       'work_email': t[8],
                                                       'country_id': self.search_country(unicode(t[9]))[0],
                                                       'state_id': self.search_state(unicode(t[10]))[0],
                                                       'district_id': self.search_district(unicode(t[11]))[0],
                                                       'ward_id': self.search_ward(unicode(t[12]))[0],
                                                       'street': t[13],
                                                       'identification_id': t[14],
                                                       'passport_id': t[15],
                                                       # 'bank_account_id': t[16],
                                                       'notes': t[17],
                                                       })
                teacher_id = self.create_teacher(t[0], t[8], self.search_school(unicode(t[4]))[0], principal_sms.check_phone(t[7]))
                teacher.user_id = teacher_id.id

    @api.multi
    def search_country(self, name):
        country = self.env['res.country'].sudo().search([('name', 'like', name)])
        if len(country) > 0:
            return country.ids
        else:
            raise ValidationError(u'Không tìm thấy Quốc gia {}'.format(name))

    @api.multi
    def search_state(self, name):
        state = self.env['res.country.state'].sudo().search([('name', 'like', name)])
        if len(state) > 0:
            return state.ids
        else:
            raise ValidationError(u'Không tìm thấy Tỉnh/Thành phố {}'.format(name))

    @api.multi
    def search_district(self, name):
        district = self.env['res.country.district'].sudo().search([('name', 'like', name)])
        if len(district) != 0:
            return district.ids
        else:
            raise ValidationError(u'Không tìm thấy Quận/Huyện {}'.format(name))

    @api.multi
    def search_ward(self, name):
        ward = self.env['res.country.ward'].sudo().search([('name', 'like', name)])
        if len(ward):
            return ward.ids
        else:
            raise ValidationError(u'Không tìm thấy Xã/Phường {}'.format(name))

    @api.multi
    def search_school(self, name):
        school = self.env['res.company'].sudo().search([('name', 'like', name)])
        if len(school):
            return school.ids
        else:
            raise ValidationError(u'Không tìm thấy trường {}'.format(name))

    @api.multi
    def search_sehoot(self, name):
        sehoot = self.env['bms.sehoot.year'].sudo().search([('name', 'like', name)])
        if len(sehoot):
            return sehoot.ids
        else:
            raise ValidationError(u'Không tìm thấy năm học {}'.format(name))

    @api.multi
    def search_study_class(self, name):
        study_class = self.env['bms.study.class'].sudo().search([('name', 'like', name)])
        if len(study_class):
            return study_class.ids
        else:
            raise ValidationError(u'Không tìm thấy lớp {}'.format(name))

    @api.multi
    def gender(self, gender):
        if gender == 'Nam' or gender == 'nam':
            return 'male'
        else:
            return 'female'

    @api.multi
    def marital(self, marital):
        if marital == u'Độc thân':
            return 'single'
        elif marital == u'Đã kết hôn':
            return 'married'
        elif marital == u'Đã qua':
            return 'widower'
        elif marital == u'Ly hôn':
            return 'divorced'

    @api.multi
    def create_teacher(self, name, work_mail, school_id, mobile_phone):
        password = randint(100000, 999999)
        user_value = {
            'name': name,
            'login': work_mail,
            'password': password,
            'email': work_mail,
            'company_ids': [(6, False, [school_id])],
            'company_id': school_id,
            'groups_id': [(6, False, [self.env.ref(
                'bms_school_report.group_user_teacher').id])],
        }
        user_id = self.env['res.users'].sudo().create(user_value)
        self.send_sms_create_user(mobile_phone, work_mail, password)
        return user_id

    @api.multi
    def create_parent(self, name, work_mail, school_id, mobile):
        password = randint(100000, 999999)
        user_value = {
            'name': name,
            'login': work_mail,
            'password': password,
            'email': work_mail,
            'company_ids': [(6, False, [school_id])],
            'company_id': school_id,
            'groups_id': [(6, False, [self.env.ref(
                'bms_school_report.group_user_parent').id])],
        }
        user_id = self.env['res.users'].sudo().create(user_value)
        # self.send_sms_create_user(mobile, user_id.email, password)
        return user_id

    @api.multi
    def send_sms_create_user(self, mobile, email, password):
        try:
            message = 'Tai khoan dang nhap DreamSchool cua ban da duoc tao bang ten dang nhap la: ' + email + ' va mat khau la: ' + str(password)
            print(message)
            p = {
                'name': 'send sms create user',
                'school_id': self.id,
                'date_sending': datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                'state': 'processing',
                'accent_vietnamese': 'no_accent',
                'message_no_accent': message,
            }

            principal = self.env['principal.sms'].sudo().create(p)
            val = {
                'to_number': mobile,
                'message': message,
                'principal_management_id': principal.id,
            }
            message = self.env['bms.sms.compose'].sudo().create(val)
            send = message.send_entity_teacher()
            if send:
                code = 1
                principal.state = 'done'
            else:
                code = -1
                principal.state = 'error'
            log = {
                'name': principal.message,
                'to_number': mobile,
                'date': principal.date_sending,
                'code': code,
                'status': principal.state,
                'teacher_id': self.env.user.id,
                'principle_sms_management_id': principal.id

            }
            self.env['bms.sms.log'].sudo().create(log)
            return True
        except:
            return False