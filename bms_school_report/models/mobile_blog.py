# -*- coding: utf-8 -*-
from odoo import fields, models, api
import datetime

class MobileBlog(models.Model):
    _name = 'mobile.blog'
    _description = 'News for mobile'

    name = fields.Char(string='Tiêu đề')
    description = fields.Text('Nội dung chi tiết')
    date = fields.Datetime(datedefault=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    attachment_id = fields.Many2one('ir.attachment', string='Ảnh')
    imageURL = fields.Char()

    @api.onchange('attachment_id')
    def create_url(self):
        if self.attachment_id:
            attachment_id = self.attachment_id.id
            self.env['ir.attachment'].browse(attachment_id).write({'public': True})
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            print(base_url)
            if attachment_id:
                self.imageURL = base_url+'/web/content/'+str(attachment_id)
