# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime, date, time
# from dateutil.relativedelta import relativedelta
# import pytz
# from datetime import timedelta


class SMSTeacherPerMonth(models.Model):
    _name = 'sms.teacher.per.month'
    _description = 'SMS Teacher Per Month'

    teacher_id = fields.Many2one('hr.employee')
    school_id = fields.Many2one('res.company')
    sms_provided_type = fields.Selection([('auto', 'Tự động'),
                                          ('manual', 'Thủ công')], default='auto')
    year = fields.Integer(
        default=lambda self: int(datetime.now().year), required=True,
        store=True)

    month = fields.Selection(
        [('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
         ('4', 'Tháng 4'),
         ('5', 'Tháng 5'), ('6', 'Tháng 6'), ('7', 'Tháng 7'),
         ('8', 'Tháng 8'),
         ('9', 'Tháng 9'), ('10', 'Tháng 10'), ('11', 'Tháng 11'),
         ('12', 'Tháng 12')])

    sms_numbers_limit = fields.Integer()
    date_provided = fields.Datetime(default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    _sql_constraints = [
        ('sms_teacher_per_month_uniq', 'unique(school_id, teacher_id, year, month)',
         'Tháng bạn đang chọn đã được cài đặt số lượng tin nhắn giới hạn! '
         'Nếu cần thay đổi thông tin cài đặt, hãy thực hiện chỉnh sửa bằng tay')
    ]

    # @api.model
    # def write(self, vals):
    #     print '------------------------'
    #     print vals
    #     return super(SMSTeacherPerMonth)

    @api.onchange('school_id', 'teacher_id')
    def sms_numbers_limit_change(self):
        if not self.school_id:
            return False
        if not self.teacher_id:
            return False
        if self.teacher_id.is_homeroom_teacher:
            self.sms_numbers_limit = self.school_id.sms_numbers_teacher
        elif self.teacher_id.is_head_master:
            self.sms_numbers_limit = self.school_id.sms_numbers_headmaster
        else:
            self.sms_numbers_limit = -1


    @api.model
    def recalculate_sms_numbers(self):
        domain_teacher = [('active', '=', True)]
        teachers = self.env['hr.employee'].search(domain_teacher)
        for t in teachers:
            date_now = datetime.now()
            year_now = date_now.year
            month_now = str(date_now.month)
            domain_1 = [('school_id', '=', t.school_id.id), ('teacher_id', '=', t.id), ('year', '=', year_now), ('month', '=', month_now)]
            already_exist = self.env['sms.teacher.per.month'].search(domain_1)
            if not already_exist:
                if t.is_homeroom_teacher:
                    sms_number_value = {
                            'school_id': t.school_id.id,
                            'teacher_id': t.id,
                            'year': year_now,
                            'month': month_now,
                            'sms_provided_type': 'auto',
                            'sms_numbers_limit': t.school_id.sms_numbers_teacher,
                            'date_provided': date_now,
                        }
                    self.env['sms.teacher.per.month'].create(sms_number_value)
                elif t.is_head_master:
                    sms_number_value = {
                        'school_id': t.school_id.id,
                        'teacher_id': t.id,
                        'year': year_now,
                        'month': month_now,
                        'sms_provided_type': 'auto',
                        'sms_numbers_limit': t.school_id.sms_numbers_headmaster,
                        'date_provided': date_now,
                    }
                    self.env['sms.teacher.per.month'].create(sms_number_value)
                elif t.is_management_system:
                    sms_number_value = {
                        'school_id': t.school_id.id,
                        'teacher_id': t.id,
                        'year': year_now,
                        'month': month_now,
                        'sms_provided_type': 'auto',
                        'sms_numbers_limit': t.school_id.sms_numbers_headmaster,
                        'date_provided': date_now,
                    }
                    self.env['sms.teacher.per.month'].create(sms_number_value)
                else:
                    sms_number_value = {
                        'school_id': t.school_id.id,
                        'teacher_id': t.id,
                        'year': year_now,
                        'month': month_now,
                        'sms_provided_type': 'auto',
                        'sms_numbers_limit': -1,
                        'date_provided': date_now,
                    }
                    self.env['sms.teacher.per.month'].create(sms_number_value)
