# -*- coding: utf-8 -*-

from odoo import models, api, _
from odoo.exceptions import ValidationError
import datetime

try:
    import tempfile
    import xlrd
except Exception:
    raise ValidationError(_('xlrd is required to install this module'))


class ReadExcel(models.Model):
    _name = 'read.excel'
    _auto = False

    @api.multi
    def read_file(self, data, sheet, path=False):
        return read_file(data, sheet, path)


def read_file(data, sheet, path=False):
    if not path:
        path = '/file.xlsx'
    try:
        file_path = tempfile.gettempdir() + path
        f = open(file_path, 'wb')
        f.write(data.decode('base64'))
        f.close()

        workbook = xlrd.open_workbook(file_path)
    except Exception:
        raise ValidationError(
            _('File format incorrect, please upload file *.xlsx format'))

    try:
        worksheet = workbook.sheet_by_name(sheet)
    except Exception:
        try:
            worksheet = workbook.sheet_by_index(0)
        except Exception:
            raise ValidationError(_('Sheet name incorrect, please upload file '
                                    'has sheet name {}'.format(sheet)))
    values = []
    for row in range(1, worksheet.nrows, 1):
        col_value = []
        for col in range(worksheet.ncols):
            value = worksheet.cell(row, col).value
            try:
                value = str(int(value))
            except:
                pass
            col_value.append(value)
        values.append(col_value)
    return values
