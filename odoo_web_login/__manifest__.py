# -*- encoding: utf-8 -*-
##############################################################################
#
#    Login Thu Vien Dien Tu
#    Copyright (C) 2018- BMSGlobal
##############################################################################
{
    'name': 'Login Screen',
    'summary': 'Login Screen For Thu Vien Dien Tu',
    'version': '1.0',
    'category': 'Website',
    'summary': """
The new configurable Odoo Web Login Screen
""",
    'author': "tuanna",
    'website': 'http://www.odooglobal.com',
    'license': 'AGPL-3',
    'depends': [
    ],
    'data': [
        'data/ir_config_parameter.xml',
        'templates/webclient_templates.xml',
        'templates/website_templates.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
}
