# coding: utf-8
from json import dumps as json
from odoo import http
from odoo.addons.web.controllers import main
from odoo import SUPERUSER_ID
from odoo.addons.web.controllers.main import serialize_exception
from random import choice, randint
from datetime import datetime
import time
import ast


class OdooApi(http.Controller):

    @http.route('/api', type='http', auth='none')
    def form_login(self):
        return http.request.render('sll_api.api_login')

    @http.route('/api/auth', type='http',
                auth='none', methods=['POST'], csrf=False)
    @serialize_exception
    def api_login(self, **post):
        content = {}
        if post.get('login') and post.get('pw'):
            session = http.request.session
            # print(session)

            # request mot session moi voi tham so DB,
            # login, password
            login = http.request.session.authenticate(
                session['db'], post['login'], post['pw'])

            # lay du lieu tu cac API duoc authenticate
            if not login:
                content['status'] = 'denied'
                return http.request.make_response(
                    json(content), [('Access-Control-Allow-Origin', '*')])

            users = http.request.env['res.users'].sudo().search([
                ('login', '=', post['login'])])

            if users:
                for user in users:
                    uid_ = user['id']
                    if not user['token']:
                        token = self.get_token(uid_)

                    hr_id = http.request.env['hr.employee'].sudo(uid_).search([('user_id', '=', int(uid_))])

                    if (len(hr_id) > 0):
                        study_class_id = http.request.env['bms.study.class'].sudo(uid_).search(['&',
                                                                                                ('active', '=', True),
                                                                                                ('homeroom_teacher', '=',
                                                                                                hr_id.id)])
                        school_id = study_class_id.school_id.id
                    else:
                        study_class_id = http.request.env['bms.study.class'].sudo(uid_).search([('active', '=', True)])
                        school_id = study_class_id.school_id.id

                    # print(hr_id.school_id.id)

                    sehoot_year_id = http.request.env['bms.sehoot.year'].sudo().search([('active', '=', True)])

                    if (len(study_class_id)>0):
                        content = {
                            'name': user['name'] or '',
                            'login': post['login'] or '',
                            'token': user['token'] or token,
                            'school_id': school_id,
                            'sehoot_year_id': sehoot_year_id.id,
                            'study_class_id': study_class_id.id,
                            'gender': hr_id.gender
                        }
                    else:
                        content = {
                            'name': user['name'] or '',
                            'login': post['login'] or '',
                            'token': user['token'] or token,
                            'school_id': hr_id.school_id.id,
                            'sehoot_year_id': sehoot_year_id.id,
                            'study_class_id': '',
                            'gender': hr_id.gender
                        }

            if not content:
                content = {'status': 'error'}
            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])

    def get_token(self, uid_):
        length = 25
        char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        token = ''.join([choice(char) for i in xrange(length)])
        check_token = http.request.env['res.users'].search(
            [('token', '=', token)])

        while check_token:
            token = ''.join([choice(char) for i in xrange(length)])
            check_token = http.request.env['res.users'].search(
                [('token', '=', token)])

        res = http.request.env['res.users'].browse(uid_).sudo().write({'token': token})
        return token

    @http.route('/api/student', type='http',
                auth='user', methods=['POST'], csrf=False)
    def get_student(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                uid_ = check_token.id
                students = http.request.env['bms.student.management'].sudo(
                    uid_).search([('user_id', '=', uid_)])
                if uid_ == SUPERUSER_ID:
                    students = http.request.env['bms.student.management'].sudo(
                        uid_).search([])
                if students:
                    for s in students:
                        content[s['name']] = {
                            'student_code': s.student_code or '',
                            'gender': s.gender or '',
                            'birthday': s.birth_day or '',
                            'class': s.study_class_id.name or '',
                            'home_teacher': s.study_class_id.homeroom_teacher.name or '',
                            'parent': s.parent_id.name or '',
                        }
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/class_teacher', type='http', auth='none', methods=['POST'], csrf=False)
    def class_teacher(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                try:
                    uid_ = check_token.id
                    classs = http.request.env['bms.study.class'].sudo(uid_).search([])
                    if classs:
                        for s in classs:
                            content[s['name']] = {
                                'key': s.id,
                                'school_id': s.school_id.name,
                                'group_of_class_id': s.group_of_class_id.name,
                                'sehoot_year_id': s.sehoot_year_id.name,
                                'student_numbers': s.student_numbers,
                                'class_room_number': s.class_room_number,
                            }
                except:
                    content = {"Message": "Error"}
                    return http.request.make_response(json(content), [
                        ('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/my_sms', type='http', auth='none', methods=['POST'], csrf=False)
    def class_teacher(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                try:
                    uid_ = check_token.id
                    classs = http.request.env['mail.channel'].sudo(
                        uid_).search([])

                    if classs:
                        for s in classs:
                            content[s['name']] = {
                                'key': s.id,
                                'school_id': s.school_id.name,
                                'group_of_class_id': s.group_of_class_id.name,
                                'sehoot_year_id': s.sehoot_year_id.name,
                                'student_numbers': s.student_numbers,
                                'class_room_number': s.class_room_number,
                            }
                except:
                    content = {"Message": "Error"}
                    return http.request.make_response(json(content), [
                        ('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])

    # @http.route('/api/get_information', type='http', auth='none', methods=['POST'], csrf=False)
    # def get_information(self, **kwa):
    #     content = {}
    #     if kwa.get('token'):
    #         check_token = http.request.env['res.users'].search([(
    #             'token', '=', kwa.get('token'))])
    #         if check_token:
    #             uid_ = check_token.id
    #             if uid_ == SUPERUSER_ID:
    #                 class_inf = http.request.env['bms.study.class'].sudo(
    #                 uid_).search([])
    #                 dict ={
    #                     " Tên Lớp ": class_inf.name,
    #                     "Trường": class_inf.school_id.name,
    #                     " Khối lớp ":class_inf.group_of_class_id.name,
    #                     "Năm học ":class_inf.sehoot_year_id.name,
    #                     "Giáo viên chủ nhiệm": class_inf.homeroom_teacher.name,
    #                     "Phòng học": class_inf.class_room_number,
    #                 }
    #             return http.request.make_response(json(dict), [
    #                 ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/student/create', type='http', auth='none', methods=['POST'], csrf=False)
    def create_student(self, **kw):
        if kw.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kw.get('token'))])
            if check_token:
                uid_ = check_token.id
                check_scode = http.request.env['bms.student.management'].sudo(
                    uid_).search([('student_code', '=', kw.get('scode'))])
                if check_scode:
                    return http.request.make_response(
                        json({'Status': 'False',
                              'Message': 'Can not create student because student code exist'}),
                        [('Access-Control-Allow-Origin', '*')])
                try:
                    res = http.request.env['bms.student.management'].sudo(
                        uid_).create({'name': kw.get('name'), 'student_code': kw.get('scode')})
                except:
                    return http.request.make_response(json({
                        'Status': 403, 'Permission': 'Denied'}),
                        [('Access-Control-Allow-Origin', '*')])
                res_json = json({'Status': 200, 'Create': 'Success',
                                 'Sid': res.id, 'Student': res.name or '',
                                 'Student_code': kw['scode']})
                return http.request.make_response(
                    res_json, [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/student/del', type='http', auth='none', methods=['POST'], csrf=False)
    def del_student(self, **kw):
        if kw.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kw.get('token'))])
            if check_token:
                uid_ = check_token.id
                check_scode = http.request.env['bms.student.management'].sudo(
                    uid_).search([('student_code', '=', kw.get('scode'))])
                user = http.request.env['res.users'].search([('login', '=', kw.get('scode'))])
                if not check_scode:
                    return http.request.make_response(json(
                        {'Status': 'False', 'Message': 'Can not delete student because student not exist'}),
                        [('Access-Control-Allow-Origin', '*')])
                if uid_ != SUPERUSER_ID:
                    return http.request.make_response(json({
                        'Status': 403, 'Permission': 'Denied'}),
                        [('Access-Control-Allow-Origin', '*')])
                res = check_scode.sudo().unlink()
                res_user = user.sudo().unlink()
                res_json = json({'Status': 200, 'Deletion': 'Success', 'Student_code': kw['scode']})
                return http.request.make_response(
                    res_json, [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/student/edit/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def edit_student(self, **kw):
        pass

    @http.route('/api/deltoken', type='http', auth='user', methods=['POST'], csrf=False)
    def del_token(self, **kw):
        session = http.request.session
        user_id = session['uid']
        user = http.request.env['res.users'].browse(user_id)
        tok = user.token
        if kw.has_key('token'):
            user_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            if tok == kw['token']:  # or user_id == SUPERUSER_ID:
                res = user.sudo().write({'token': False})
                return http.request.make_response(json({
                    'Deletion': 'Success',
                    'Token': tok,
                }))
            else:
                try:
                    res = http.request.env['res.users'].browse(
                        user_token.id).sudo(user_id).write({'token': False})
                except:
                    return http.request.make_response(json({
                        'Deletion': 'False',
                        'Permission': 'Denied',
                    }))
                return http.request.make_response(json({
                    'Deletion': 'Success',
                    'Token': kw['token'],
                }))

    @http.route('/api/retoken', type='http', auth='user', methods=['POST'], csrf=False)
    def renew_token(self, **kw):
        if kw.has_key('token'):
            session = http.request.session
            user = http.request.env['res.users'].browse(session['uid'])
            user_token = http.request.env['res.users'].search(
                [('token', '=', kw['token'])])
            if user.token == kw['token'] or session['uid'] == SUPERUSER_ID:
                new_token = self.get_token(user_token.id)
                res = user_token.sudo().write({'token': new_token})
                return http.request.make_response(json({
                    'Message': 'Renew token',
                    'Token': new_token,
                }))
            return http.request.make_response(json({
                'Message': 'Permission deined',
            }))
        return http.request.make_response(json({
            'Message': 'Renew token fail',
        }))

    @http.route('/api/teacher/create', type='http', auth='none', methods=['POST'], csrf=False)
    def create_teacher(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key('email'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:
                res = http.request.env['hr.employee'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'work_email': kw['email'], })
            except:
                return http.request.make_response(json({
                    'Message': 'Permission deined',
                }))
            return http.request.make_response(json({
                'Message': 'Create teacher success',
                'name': kw['name'],
                'teacher_id': res.id or '',
            }))

    @http.route('/api/teacher', type='http', auth='none', methods=['POST'], csrf=False)
    def get_student_by_hometeacher(self, **kw):
        contx = {}
        if not kw.has_key('token'):
            return http.request.make_response(json({
                'Message': u'Cần nhập vào token'}))
        user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
        teachers = http.request.env['hr.employee'].sudo().search([])
        if not user_id:
            http.request.make_response(json({
                'Message': u'Token không chính xác. Vui lòng kiểm tra lại'}))
        classs = http.request.env['bms.study.class'].sudo(user_id.id).search([])
        if not teachers:
            http.request.make_response(json({
                'Message': u'Không có giáo viên'}))
        for teacher in teachers:
            if teacher.user_id.id == user_id.id:
                user_teacher = teacher.id
        for cls in classs:
            if cls.homeroom_teacher.id == user_teacher:
                if cls.student_ids:
                    res = {}
                    for std in cls.student_ids:
                        res[std.id] = {'name': std.name,
                                       'gender': std.gender,
                                       'key': std.id,
                                       'student_code': std.student_code,
                                       'gender': std.gender,
                                       'birthday': std.birth_day,
                                       'class': std.study_class_id.name,
                                       'home_teacher': std.study_class_id.homeroom_teacher.name,
                                       'parent': std.parent_id.name
                                       }
                    contx.update({cls.id: res})
        return http.request.make_response(json(contx))

    @http.route('/api/class/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def get_class(self, **kw):
        if kw.has_key('id') and kw.has_key('token'):
            user = http.request.env['res.users'].search([('token', '=', kw['token'])])
            if user:
                class_ = http.request.env['bms.study.class'].sudo(user.id).search([('id', '=', kw['id'])])
                if class_:
                    contx = {'Message': 'Class infomation', 'id': class_.id,
                             'class': class_.name}
                    student_ = {}
                    for student in class_.student_ids:
                        student_.update({
                            student.id: student.name or ''})
                    contx['students'] = student_
                    return http.request.make_response(json(contx),
                                                      [('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(
                    json({'Message': 'No permission or class id invalid! Check again'}),
                    [('Access-Control-Allow-Origin', '*')])
            return http.request.make_response(json({'Message': 'Token invalid'}),
                                              [('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json({
            'Message': 'Have not id or token. Please check again!',
        }))

    @http.route('/api/school/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def get_all_class(self, **kw):
        if kw.has_key('id') and kw.has_key('token'):
            user = http.request.env['res.users'].search(
                [('token', '=', kw['token'])])
            if user:
                all_class = http.request.env['bms.study.class'].sudo().search(
                    [('active', '=', True), ('school_id', '=', int(kw['id']))])
                if all_class:
                    contx = {}
                    for school in user.company_ids:
                        class_ = {}
                        for cls in all_class:
                            if school.id == cls.school_id.id:
                                class_[cls.id] = {
                                    'class_name': cls.name,
                                    'home_teacher': cls.homeroom_teacher.sudo().name or '',
                                    'home_teacher_id': cls.homeroom_teacher.id or '', }
                        contx.update({
                            school.id: {school.name: class_}
                        })
                    return http.request.make_response(json(contx))
                return http.request.make_response(json(
                    {'Message': 'Have not school'}))
            return http.request.make_response(json(
                {'Message': 'Your token or id school invalid'}
            ))
        return http.request.make_response(json(
            {'Message': 'Please input school id and token'}))

    @http.route('/api/groupsms', type='http', auth='none', methods=['POST'], csrf=False)
    def get_group_student(self, **kw):
        contx = {}
        if not kw.has_key('token'):
            return http.request.make_response(json({
                'Message': 'Not have token key. Please check again!'}))
        user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
        if not user_id:
            return http.request.make_response(json({
                'Message': 'Token invalid'}))
        try:
            group_students = http.request.env['group.student'].sudo(user_id.id).search([])
        except:
            return http.request.make_response(json({
                'Message': 'You not have permission or not have group student'}))
        for group in group_students:
            if user_id.company_id.id == group.school_id.id:
                contx[group.id] = {
                    'key': group.id or '',
                    'group': group.name or '',
                    'class': group.study_class_id.name or '',
                    'student_numbers': group.student_numbers or '',
                    'student_ids': group.student_ids.ids or ''}
        return http.request.make_response(json(contx))

    @http.route('/api/sms/create_all', type='http', auth='none', methods=['POST'], csrf=False)
    def create_sms(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key('school_id') and kw.has_key(
                'sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key('date_sending') and kw.has_key(
            'message_no_accent'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            res1 = http.request.env['sms.management'].sudo(user_id.id).search([('id', '=', 2)])
            print(res1.date_sending)
            try:
                res = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'is_send_all': True,
                    'message_no_accent': kw['message_no_accent'],
                    'accent_vietnamese': 'no_accent',
                })
                res._send_all_change()
                res.action_send()
            except:
                return http.request.make_response(json({
                    'Message': 'Permission deined',
                }))
            return http.request.make_response(json({
                'Message': 'Create sms success',
                'name': kw['name'],
            }))

    @http.route('/api/sms/create_t', type='http', auth='none', methods=['POST'], csrf=False)
    def create_teacher(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key(
                'school_id') and kw.has_key('sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key(
            'date_sending') and kw.has_key('message_no_accent'):
            id = kw['group_student_ids'].split(',')
            id = map(lambda x: int(x), id)
            # id = kw['group_student_ids'].encode('ascii', 'ignore')
            thamsogroup = http.request.env['group.student'].browse(id)
            print("*" * 30)
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:

                request = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'message_no_accent': kw['message_no_accent'],
                    'group_student_ids': (1, 2),
                    'accent_vietnamese': 'no_accent',
                    'is_send_all': False,
                })
                print("*" * 30)
                print(kw['group_student_ids'])
                print(request.name)
                request.group_student_ids = thamsogroup
                request._send_group_change()
                request.action_send()

            except:
                return http.request.make_response(json({
                    'Message': 'Lỗi không gửi được',
                }))
            return http.request.make_response(json({
                'Message': 'Gửi thành công',
            }))

    @http.route('/api/sms/create_one', type='http', auth='none', methods=['POST'], csrf=False)
    def create_one(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key(
                'school_id') and kw.has_key('sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key(
            'date_sending') and kw.has_key('message_no_accent') and kw.has_key('student_ids'):
            id = kw['student_ids'].split(',')
            id = map(lambda x: int(x), id)
            # id = kw['student_ids'].encode('ascii', 'ignore')
            print(id)
            thamsostudent = http.request.env['bms.student.management'].browse(id)
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:

                request = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'message_no_accent': kw['message_no_accent'],
                    'is_send_all': False,
                    'accent_vietnamese': kw['accent_vietnamese'],
                    'student_ids': kw['student_ids'],
                })
                request.student_ids = thamsostudent
                request.action_send()

            except:
                return http.request.make_response(json({
                    'Message': 'Lỗi không gửi được',
                }))
            return http.request.make_response(json({
                'Message': 'Gửi thành công',
            }))

    @http.route('/api/sms/get_images', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        id = kw['study_class_id'].split(',')
        id = map(lambda x: int(x), id)
        if user:
            try:
                getimage = http.request.env['upload.images'].sudo().search([('study_class_id', '=', id)])
                print(getimage)
                for s in getimage:
                    content[s['id']] = {
                        'name': s.name,
                        'URL Images': s.imageURL,
                    }
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_images_base64', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images_from_id(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        id_upload_images = int(kw['id_upload_images'])
        print(user)
        if user:
            try:
                upload_image = http.request.env['upload.images'].sudo().browse([id_upload_images])
                ir_attactment_file = upload_image.attachment_id
                # print(ir_attactment_file.datas)
                content = {
                    'datas': ir_attactment_file.datas
                }
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/history', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_history(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            gethistory = http.request.env['sms.management'].sudo().search([])
            # print(gethistory.id)
            if gethistory:
                print(gethistory)
                for i in gethistory:
                    print(i)
                    content[i['id']] = {
                        'key': i.id,
                        'message_no_accent': i.message_no_accent,
                        'study_class_id': i.study_class_id.name,
                        'date_sending': i.date_sending,
                        'name': i.name,
                    }

            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])
        except:
            content = {"Message": "Error"}
            return http.request.make_response(json(content))

    @http.route('/api/sms/create_images', type='http', auth='none', methods=['POST'], csrf=False)
    def create_images(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key('namedatas') and kw.has_key('date') \
                and kw.has_key('datas_fname') and kw.has_key('datas'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:
                datas = kw['datas'].encode('ascii', 'ignore')
                create_data = http.request.env['ir.attachment'].sudo(user_id.id).create({
                    'name': kw['namedatas'],
                    'datas': datas,
                    'datas_fname': kw['datas_fname'],
                    'res_model': 'res.users',
                    'type': 'binary',
                })
                id = create_data.id
                print(kw['school_id'])

                if kw.has_key('is_sent_all_school'):
                    request = http.request.env['upload.images'].sudo(user_id.id).create({
                        'name': kw['name'],
                        'attachment_id': id,
                        'date': kw['date'],
                        'school': kw['school_id'],
                        'is_sent_all_school': True
                    })

                else:
                    request = http.request.env['upload.images'].sudo(user_id.id).create({
                        'name': kw['name'],
                        'study_class_id': kw['study_class_id'],
                        'attachment_id': id,
                        'date': kw['date'],
                        'school': kw['school_id'],
                    })

                request.create_url()

            except:
                return http.request.make_response(json({
                    'Message': 'Error',
                }))
        return http.request.make_response(json({
            'Message': 'Done'}))

    @http.route('/api/sms/send_school', type='http', auth='none', methods=['POST'], csrf=False)
    def send_school(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        sendschool = http.request.env['res.company'].sudo(user.id).search([])
        searchclass = http.request.env['bms.study.class'].sudo(user.id).search([])
        # searchstudent = http.request.env['bms.student.management'].sudo().search([('school_id', '=', sendschool.id)])
        searchclass = map(lambda x: int(x), searchclass)
        for i in searchclass:
            request = http.request.env['sms.management'].sudo().create({
                'name': kw['name'],
                'school_id': sendschool.id,
                'sehoot_year_id': kw['sehoot_year_id'],
                'study_class_id': i,
                'date_sending': kw[''
                                   ''],
                'message_no_accent': kw['message_no_accent'],
                'is_send_all': True,
                'accent_vietnamese': 'no_accent',
                'student_ids': '',
            })
            request._send_all_change()
            request.action_send()
        return http.request.make_response(json({
            'Message': 'Done'
        }), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/head_master', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_head_master(self, **kw):
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            principal = http.request.env['principal.sms'].sudo().create({
                'name': kw['title'],
                'school_id': user.company_ids.id,
                'date_sending': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'state': 'processing',
                'accent_vietnamese': kw['accent'],
            })
            val = {
                'to_number': kw['mobiless'],
                'message': kw['message'],
                'principal_management_id': principal.id,
            }
            message = http.request.env['bms.sms.compose'].sudo().create(val)
            send = message.send_entity_teacher()
            if send:
                return http.request.make_response(json({
                    'Message': 'Done'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
            else:
                return http.request.make_response(json({
                    'Message': 'Error'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/send_all_teachers', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_head_master_all_teachers(self, **kw):
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            message_no_accent = ''
            if kw['accent'] == 'no_accent':
                message_no_accent = kw['message']

            principal = http.request.env['principal.sms'].sudo().create({
                'name': kw['title'],
                'school_id': user.company_ids.id,
                'date_sending': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'state': 'processing',
                'accent_vietnamese': kw['accent'],
                'is_send_all_teachers': kw['is_send_all_teachers'],
                'message_no_accent': message_no_accent,

            })

            if principal.is_send_all_teachers:
                domain = [('school_id', '=', principal.school_id.id)]
                teacher_id = http.request.env['hr.employee'].sudo().search(domain).ids
                if teacher_id:
                    principal.teacher_ids = [(6, False, teacher_id)]
            else:
                principal.teacher_ids = []

            mobiles = []
            for teacher in principal.teacher_ids:
                if teacher.mobile_phone:
                    mobile = principal._check_mobile(teacher.mobile_phone)
                    mobiles.append(mobile)
                mobiless = ",".join((mobile) for mobile in mobiles)

            val = {
                'to_number': mobiless,
                'message': kw['message'],
                'principal_management_id': principal.id,
            }
            message = http.request.env['bms.sms.compose'].sudo().create(val)
            print(message)
            send = message.send_entity_teacher()
            code = 0
            if send:
                principal.write({'state': 'done'})
                code = 1
            else:
                principal.write({'state': 'error'})
                code = -1

            log = {
                'name': principal.message,
                'to_number': mobiles,
                'date': principal.date_sending,
                'code': code,
                'status': principal.state,
                'teacher_id': user.id,
                'principle_sms_management_id': principal.id

            }
            http.request.env['bms.sms.log'].sudo().create(log)
            if send:
                return http.request.make_response(json({
                    'Message': 'Done'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
            else:
                return http.request.make_response(json({
                    'Message': 'Error'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/send_all_students', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_head_master_all_students(self, **kw):
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            message_no_accent = ''
            if kw['accent'] == 'no_accent':
                message_no_accent = kw['message']

            principal = http.request.env['principal.sms'].sudo().create({
                'name': kw['title'],
                'school_id': user.company_ids.id,
                'date_sending': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'state': 'processing',
                'accent_vietnamese': kw['accent'],
                'is_send_all_students': kw['is_send_all_students'],
                'message_no_accent': message_no_accent,

            })

            if principal.is_send_all_students:
                domain = [('school_id', '=', principal.school_id.id)]
                parent_ids = http.request.env['res.partner'].sudo().search(domain).ids
                if parent_ids:
                    # teacher_id = map(lambda x: int(x), teacher_id)
                    principal.parent_ids = [(6, False, parent_ids)]
            else:
                principal.parent_ids = []

            mobiles = []
            for parents in principal.parent_ids:
                if parents.mobile:
                    mobile = principal._check_mobile(parents.mobile)
                    mobiles.append(mobile)
                mobiless = ",".join((mobile) for mobile in mobiles)

            val = {
                'to_number': mobiless,
                'message': kw['message'],
                'principal_management_id': principal.id,
            }
            message = http.request.env['bms.sms.compose'].sudo().create(val)
            send = message.send_entity_teacher()
            if send:
                principal.write({'state': 'done'})
                code = 1
            else:
                principal.write({'state': 'error'})
                code = -1

            log = {
                'name': principal.message,
                'to_number': mobiles,
                'date': principal.date_sending,
                'code': code,
                'status': principal.state,
                'teacher_id': user.id,
                'principle_sms_management_id': principal.id

            }
            http.request.env['bms.sms.log'].sudo().create(log)
            if send:
                principal.write({'state': 'done'})
            else:
                principal.write({'state': 'error'})
        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/getnewpass', type='http', auth='none', methods=['POST'], csrf=False)
    def getnewpass(self, **kw):
        try:
            employee_ids = http.request.env['hr.employee'].sudo().search(['|',
                                                                         ('work_email', '=', kw['forgot_info']),
                                                                         ('mobile_phone', '=', kw['forgot_info'])
                                                                         ])
            if employee_ids:
                for employee_id in employee_ids:
                    if parent.user_id:
                        password = randint(100000, 999999)
                        parent.user_id.password = password
                        school = parent.school_id
                        send_sms = school.send_sms_create_user(kw['mobile'], parent.user_id.login, password)
                        if send_sms:
                            return http.request.make_response(json({
                                'Message': 'Gui tin nhan thanh cong'
                            }), [
                                ('Access-Control-Allow-Origin', '*')])
                        else:
                            return http.request.make_response(json({
                                'Message': 'gui tin nhan that bai'
                            }), [
                                ('Access-Control-Allow-Origin', '*')])
                    else:
                        return http.request.make_response(json({
                            'Message': 'Khong co nguoi dung voi so dien thoai vua nhap'
                        }), [
                            ('Access-Control-Allow-Origin', '*')])
            else:
                return http.request.make_response(json({
                    'Message': 'Khong tim thay so dien thoai'
                }), [
                    ('Access-Control-Allow-Origin', '*')])

        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/parent_get_password', type='http', auth='none', methods=['POST'], csrf=False)
    def parent_get_password(self, **kw):
        try:
            parents = http.request.env['res.partner'].sudo().search([('phone', '=', kw['mobile'])])
            if parents:
                for parent in parents:
                    if parent.user_id:
                        password = randint(100000, 999999)
                        parent.user_id.password = password
                        school = parent.school_id
                        send_sms = school.send_sms_create_user(kw['mobile'], parent.user_id.login, password)
                        if send_sms:
                            return http.request.make_response(json({
                                'Message': 'Gui tin nhan thanh cong'
                            }), [
                                ('Access-Control-Allow-Origin', '*')])
                        else:
                            return http.request.make_response(json({
                                'Message': 'gui tin nhan that bai'
                            }), [
                                ('Access-Control-Allow-Origin', '*')])
                    else:
                        return http.request.make_response(json({
                            'Message': 'Khong co nguoi dung voi so dien thoai vua nhap'
                        }), [
                            ('Access-Control-Allow-Origin', '*')])
            else:
                return http.request.make_response(json({
                    'Message': 'Khong tim thay so dien thoai'
                }), [
                    ('Access-Control-Allow-Origin', '*')])

        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/send_all_school', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_head_master_all_school(self, **kw):
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            message_no_accent = ''
            if kw['accent'] == 'no_accent':
                message_no_accent = kw['message']

            principal = http.request.env['principal.sms'].sudo().create({
                'name': kw['title'],
                'school_id': user.company_ids.id,
                'date_sending': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'state': 'processing',
                'accent_vietnamese': kw['accent'],
                'is_send_all_school': kw['is_send_all_school'],
                'message_no_accent': message_no_accent,

            })

            if principal.is_send_all_school:
                domain = [('school_id', '=', principal.school_id.id)]
                teacher_id = http.request.env['hr.employee'].sudo().search(domain).ids
                if teacher_id:
                    # teacher_id = map(lambda x: int(x), teacher_id)
                    principal.teacher_ids = [(6, False, teacher_id)]

                parent_ids = http.request.env['res.partner'].sudo().search(domain).ids
                if parent_ids:
                    # teacher_id = map(lambda x: int(x), teacher_id)
                    principal.parent_ids = [(6, False, parent_ids)]
            else:
                principal.teacher_ids = []
                principal.parent_ids = []

            mobiles = []
            for teacher in principal.teacher_ids:
                if teacher.mobile_phone:
                    mobile = principal._check_mobile(teacher.mobile_phone)
                    mobiles.append(mobile)
                mobiless = ",".join((mobile) for mobile in mobiles)

            for parent in principal.parent_ids:
                if parent.mobile:
                    mobile = principal._check_mobile(parent.mobile)
                    mobiles.append(mobile)
                mobiless = ",".join((mobile) for mobile in mobiles)
            val = {
                'to_number': mobiless,
                'message': kw['message'],
                'principal_management_id': principal.id,
            }
            message = http.request.env['bms.sms.compose'].sudo().create(val)
            send = message.send_entity_teacher()
            code = 0
            if send:
                principal.write({'state': 'done'})
                code = 1
            else:
                principal.write({'state': 'error'})
                code = -1

            log = {
                'name': principal.message,
                'to_number': mobiles,
                'date': principal.date_sending,
                'code': code,
                'status': principal.state,
                'teacher_id': user.id,
                'principle_sms_management_id': principal.id

            }
            print(log)
            http.request.env['bms.sms.log'].sudo().create(log)
            if send:
                return http.request.make_response(json({
                    'Message': 'Done'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
            else:
                return http.request.make_response(json({
                    'Message': 'Error'
                }), [
                    ('Access-Control-Allow-Origin', '*')])
        except:
            return http.request.make_response(json({
                'Message': 'Error'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/blog/get_info', type='http', auth='none', methods=['POST'], csrf=False)
    def get_info(self, **kw):
        content = {}
        content['noi_dung'] = []
        try:
            domain = [('website_published', '=', False)]
            blog_ids = http.request.env['blog.post'].sudo().search(domain)
            for blog_id in blog_ids:
                content['noi_dung'].append({
                    'id': blog_id.id or '',
                    'name': blog_id.name or '',
                    'subtitle': blog_id.subtitle or '',
                    'author': blog_id.author_id.name or '',
                    'create_date': blog_id.create_date or '',
                    'published_date': blog_id.post_date or '',
                    'last_contributor': blog_id.write_uid.name or '',
                    'last_modification': blog_id.write_date or ''})

            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])
        except:
            content = {"Message": "Error"}
            return http.request.make_response(json(content))

    @http.route('/api/get_infomation_more', type='http', auth='none', methods=['POST'], csrf=False)
    def get_infomation_more(self, **kw):
        content = {}
        info = http.request.env['more.infomation'].sudo().search([])
        content[info.id] = {
            'website': info.website or '',
            'facebook': info.facebook or '',
            'email': info.email or '',
            'phone': info.phone or '',
            'address': info.address or '',
            'intro': info.intro or '',
        }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_my_sms', type='http', auth='none', methods=['POST'], csrf=False)
    def get_my_sms(self, **kw):
        content = {}
        content['danhsach'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            id_student = http.request.env['bms.student.management'].sudo().search([('user_id', '=', user.id)])
            sms_ids = id_student.tin_nhan_ids
            for sms_id in sms_ids:
                content['danhsach'].append(sms_id.id)
        except:
            content['danhsach'] = []
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_content_my_sms', type='http', auth='none', methods=['POST'], csrf=False)
    def get_content_sms(self, **kw):
        content = {}
        content['noi_dung'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        sms_can_lay = ast.literal_eval(kw['sms_ids'])
        try:
            for sms_id in sms_can_lay:
                noidungtin = {}
                noidungtin['key'] = sms_id
                sms_mana = http.request.env['sms.management'].sudo().search([('id', '=', sms_id)])
                noidungtin['noi_dung'] = sms_mana.message_no_accent or sms_mana.message_accent
                noidungtin['time'] = sms_mana.date_sending
                content['noi_dung'].append(noidungtin)
        except:
            content['noi_dung'] = []
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_images_ids', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images_ids(self, **kw):
        content = {}
        content['noi_dung'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        id_class = int(kw['study_class_id'])
        if user:
            try:
                hoatdong_ids = http.request.env['upload.images'].sudo().search([('study_class_id', '=', id_class)])
                print(hoatdong_ids)
                for hoatdong_id in hoatdong_ids:
                    content['noi_dung'].append(hoatdong_id.id)
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_images_ids_content', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images_ids_content(self, **kw):
        content = {}
        content['noi_dung'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        hoat_dong_ids = ast.literal_eval(kw['hoat_dong_ids'])
        if user:
            try:
                for hoat_dong_id in hoat_dong_ids:
                    noidungtin = {}
                    hoat_dong = http.request.env['upload.images'].sudo().search([('id', '=', hoat_dong_id)])
                    noidungtin['key'] = hoat_dong_id
                    noidungtin['date'] = hoat_dong.date
                    noidungtin['name'] = hoat_dong.name
                    noidungtin['url_images'] = hoat_dong.imageURL
                    content['noi_dung'].append(noidungtin)
            except:
                content['noi_dung'] = []
        else:
            content['noi_dung'] = []
        return http.request.make_response(json(content), [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_blog_ids', type='http', auth='none', methods=['POST'], csrf=False)
    def get_blog_ids(self, **kw):
        content = {}
        content['noi_dung'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        if user:
            try:
                blog_ids = http.request.env['mobile.blog'].sudo().search([])
                for blog_id in blog_ids:
                    content['noi_dung'].append(blog_id.id)
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_blog_ids_content', type='http', auth='none', methods=['POST'], csrf=False)
    def get_blog_ids_content(self, **kw):
        content = {}
        content['noi_dung'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        blog_ids = ast.literal_eval(kw['blog_ids'])
        if user:
            try:
                for blog_id in blog_ids:
                    noidungtin = {}
                    hoat_dong = http.request.env['mobile.blog'].sudo().search([('id', '=', blog_id)])
                    noidungtin['key'] = blog_id
                    noidungtin['date'] = hoat_dong.date
                    noidungtin['name'] = hoat_dong.name
                    noidungtin['url_images'] = hoat_dong.imageURL
                    noidungtin['description'] = hoat_dong.description
                    content['noi_dung'].append(noidungtin)
            except:
                content['noi_dung'] = []
        else:
            content['noi_dung'] = []
        return http.request.make_response(json(content), [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/trao_doi/create', type='http', auth='none', methods=['POST'], csrf=False)
    def create_traodoi(self, **kw):
        if kw.has_key('token') and kw.has_key('mail_channel_id') and kw.has_key('body'):
            # list_mail_channel = http.request.env['mail.channel'].sudo().search([])
            # print(list_mail_channel)
            mail_channel_id = int(kw['mail_channel_id'])
            user_has_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            user_partner_id = user_has_token.partner_id.id
            mail_channel = http.request.env['mail.channel'].sudo().browse(mail_channel_id)
            create_new_noi_dung_id = http.request.env['mail.message'].sudo().create({
                'author_id': user_partner_id,
                'model': 'mail.channel',
                'message_type': 'comment',
                'body': kw['body']
            })
            # print(create_new_noi_dung_id)
            create_new_noi_dung_id.channel_ids = mail_channel

            return http.request.make_response(json({
                'message': 'Create notification success in channel success',
            }))
        else:
            return http.request.make_response(json({
                'message': 'Create failure',
            }))

    @http.route('/api/list_trao_doi', type='http', auth='none', methods=['POST'], csrf=False)
    def list_traodoi(self, **kw):
        if kw.has_key('token'):
            user_has_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            user_partner_id = int(user_has_token.partner_id.id)
            mail_channel_ids = http.request.env['mail.channel'].sudo().search([])
            content = {}
            content['noi_dung'] = []
            for mail_channel_id in mail_channel_ids:
                partner_ids = mail_channel_id.channel_partner_ids
                array_partner_ids = map(lambda x: x.id, partner_ids)
                if user_partner_id in array_partner_ids:
                    content['noi_dung'].append(mail_channel_id.id)
            return http.request.make_response(json(content))
        else:
            return http.request.make_response(json({
                'noi_dung': []
            }))

    @http.route('/api/get_mail_channel_content', type='http', auth='none', methods=['POST'], csrf=False)
    def list_traodoi_content(self, **kw):
        if kw.has_key('token'):
            user_has_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            user_partner_id = int(user_has_token.partner_id.id)
            content = {}
            content['noi_dung'] = []
            mail_channel_ids = ast.literal_eval(kw['mail_channel_ids'])
            for mail_channel_id in mail_channel_ids:
                mail_channel_object = http.request.env['mail.channel'].sudo().browse(int(mail_channel_id))
                content['noi_dung'].append({
                    'key': mail_channel_id,
                    'name': mail_channel_object.name
                })
            return http.request.make_response(json(content))
        else:
            return http.request.make_response(json({
                'noi_dung': []
            }))

    @http.route('/api/list_mail_message_ids', type='http', auth='none', methods=['POST'], csrf=False)
    def list_mail_message_ids(self, **kw):
        if kw.has_key('token'):
            user_has_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            user_partner_id = int(user_has_token.partner_id.id)
            mail_channel_id = int(kw['mail_channel_id'])
            mail_message_ids = http.request.env['mail.message'].sudo().search([('channel_ids', '=', mail_channel_id)])
            content = {}
            content['noi_dung'] = map(lambda x: x.id, mail_message_ids)
            return http.request.make_response(json(content))
        else:
            return http.request.make_response(json({
                'noi_dung': []
            }))

    @http.route('/api/list_mail_message_ids_content', type='http', auth='none', methods=['POST'], csrf=False)
    def list_mail_message_ids_content(self, **kw):
        if kw.has_key('token'):
            user_has_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            user_partner_id = int(user_has_token.partner_id.id)

            mail_message_ids = ast.literal_eval(kw['mail_message_ids'])
            mail_message_ids_object = http.request.env['mail.message'].sudo().browse(mail_message_ids)
            content = {}
            content['noi_dung'] = []
            for mail_message_id in mail_message_ids_object:
                html_tag = re.compile('<.*?>')
                body_content = mail_message_id.body
                body_content = re.sub(html_tag,'',body_content)
                content['noi_dung'].append({'key': mail_message_id.id, 'body': body_content,
                    'create_date': mail_message_id.create_date})
                # content['noi_dung'] = map(lambda x: {'key': x.id, 'body': x.body,
                #     'create_date': x.create_date}, mail_message_ids_object)
            return http.request.make_response(json(content))
        else:
            return http.request.make_response(json({
                'noi_dung': []
            }))

    @http.route('/api/get_images_by_class', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images(self, **kw):
        content = {}
        content['hinh_anh'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        id_class = int(kw['study_class_id'])
        id_school = http.request.env['bms.study.class'].sudo().browse(id_class).school_id.id
        if user:
            try:
                print("aaa")
                getimage = http.request.env['upload.images'].sudo().search(
                    ["|", ('study_class_id', '=', id_class), "&", ('school', '=', id_school),
                     ('is_sent_all_school', '=', True)
                     ], order= 'date desc')
                # print("bbb")
                # getimage = http.request.env['upload.images'].sudo().search([('study_class_id', '=', id_class)])
                # print(getimage)
                # get_is_list_by_school = http.request.env['upload.images'].sudo().search(
                #     [['school', '=', id_school], ['is_sent_all_school', '=', True]], order= 'date desc')
                # print(get_is_list_by_school)
                for s in getimage:
                    content['hinh_anh'].append({
                        'key': s.id,
                        'date': s.date,
                        'name': s.name,
                        'url_images': s.imageURL,
                    })

                # for item in get_is_list_by_school:
                #      content['hinh_anh'].append({
                #          'key': item.id,
                #          'date': item.date,
                #          'name': item.name,
                #          'url_images':item.imageURL,
                #      })

                list1 = content['hinh_anh']
                print(list1)
                # abc = []
                # for item in list1:
                #     abc.append(item['date'])
                # print(item)
                # def foo(x):
                #     return time.strptime(x, '%Y-%m-%d %H:%M:%S')[0:6]
                # abc.sort(key=foo)
                # print(abc)
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/get_images_ids_by_class', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images_by_class(self, **kw):
        content = {}
        content['hinh_anh'] = []
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        id_class = int(kw['study_class_id'])
        if user:
            try:
                getimage = http.request.env['upload.images'].sudo().search([('study_class_id', '=', id_class)])
                for s in getimage:
                    content['hinh_anh'].append(s.id)
            except:
                content = {}
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/lay_thong_tin_xe', type='http', auth='none', methods=['POST'], csrf=False)
    def get_all_car_information(self, **kw):

        content = {}
        car_ids = http.request.env['tgl.quanlyxe'].sudo().search([])
        print(car_ids)
        # tgl.quanlyxe(1, 2)
        for car_id in car_ids:
            print(car_id)
            # tgl.quanlyxe(1,)
            print(car_id.name)
            # 29C-12345
            print(car_id.trongluongxe)
            # 20000.0
            car_id.trongluongxe = 30000

            print(car_id.taixe_id)
            taixe = car_id.taixe_id
            tentaixe = car_id.taixe_id.name
            # car_id.taixe_id.name = "Anh Long"
            print(taixe.name)
            # hr.employee(2,)
            content[car_id.id] = {
                'name': car_id.name or '',
                'trongluongxe': car_id.trongluongxe or '',
                'taixe_id': car_id.taixe_id.id or '',
                'ten_tai_xe': car_id.taixe_id.name or ''
            }

        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/lay_thong_tin_id_xe', type='http', auth='none', methods=['POST'], csrf=False)
    def get_specific_car_information(self, **kw):
        print(kw)
        idxe = kw['idxe']
        content = {}
        car_ids = http.request.env['tgl.quanlyxe'].sudo().search([('id', '=', idxe)])
        print(car_ids)
        # tgl.quanlyxe(1, 2)
        for car_id in car_ids:
            print(car_id)
            # tgl.quanlyxe(1,)
            print(car_id.name)
            # 29C-12345
            print(car_id.trongluongxe)
            # 20000.0
            car_id.trongluongxe = 30000

            print(car_id.taixe_id)
            taixe = car_id.taixe_id
            tentaixe = car_id.taixe_id.name
            # car_id.taixe_id.name = "Anh Long"
            print(taixe.name)
            # hr.employee(2,)
            content[car_id.id] = {
                'name': car_id.name or '',
                'trongluongxe': car_id.trongluongxe or '',
                'taixe_id': car_id.taixe_id.id or '',
                'ten_tai_xe': car_id.taixe_id.name or ''
            }

        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/tao_xe', type='http', auth='none', methods=['POST'], csrf=False)
    def get_specific_car_information(self, **kw):
        print(kw)
        bienso = kw['bienso']
        trongluong = kw['trongluong']
        content = {}
        try:
            car_new_id = http.request.env['tgl.quanlyxe'].sudo() \
                .create({'name': bienso, 'trongluongxe': trongluong})
            print(car_new_id)
            content = {
                'create': 'success'
            }
        except:
            content = {
                'create': 'fail'
            }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/cap_nhat_trong_luong', type='http', auth='none', methods=['POST'], csrf=False)
    def cap_nhat_trong_luong(self, **kw):
        print(kw)
        bienso = kw['bienso']
        trongluongmoi = kw['trongluongmoi']
        content = {}
        try:
            car_ids = http.request.env['tgl.quanlyxe'].sudo(). \
                search([('name', '=', bienso)])
            for car_id in car_ids:
                car_id.trongluongxe = trongluongmoi
            content = {
                'update': 'success'
            }
        except:
            content = {
                'update': 'fail'
            }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/cap_nhat_trong_luong_theo_id', type='http', auth='none', methods=['POST'], csrf=False)
    def cap_nhat_trong_luong_theoid(self, **kw):
        print(kw)
        idxe = int(kw['idxe'])
        trongluongmoi = kw['trongluongmoi']
        print(trongluongmoi)
        print(idxe)
        content = {}
        try:
            # tim_xe = http.request.env['tgl.quanlyxe'].sudo().browse(idxe)
            http.request.env['tgl.quanlyxe'].sudo() \
                .browse(idxe).write({"trongluongxe": trongluongmoi})
            content = {
                'update': 'success'
            }
        except:
            content = {
                'update': 'fail'
            }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/xoa_xe_theo_id', type='http', auth='none', methods=['POST'], csrf=False)
    def xoa_xe_theoid(self, **kw):
        idxe = int(kw['idxe'])
        try:
            # tim_xe = http.request.env['tgl.quanlyxe'].sudo().browse(idxe)
            http.request.env['tgl.quanlyxe'].sudo(1) \
                .browse(idxe).unlink()
            content = {
                'delete': 'success'
            }
        except:
            content = {
                'delete': 'fail'
            }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])
