# -*- coding: utf-8 -*-

from odoo import fields, models

class People_emotion(models.Model):
    _name = 'bms_computer_people_emotion'
    _description = 'people_emotion'

    IP_id = fields.Many2one('bms_computer_vision.ip_camera', string="IP_id")
    smile_time = fields.Datetime("Thời gian cười")
    smile = fields.Boolean("Đang cười ?")
    customers_id = fields.Many2one('res.partner', string="Id of customers")