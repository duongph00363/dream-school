# -*- coding: utf-8 -*-

from odoo import fields, models


class Department(models.Model):
    _inherit = 'hr.department'
    _description = 'Department BMS Computer Vision'

    ip_camera_of_department = fields.One2many('bms_computer_vision.ip_camera', 'departmentIdOfIP_Camera', string = 'IP_Camera')
    user = fields.Char("User")
    #
    # IP_OfIP_Camera = fields.Char("IP")
    # accountOfIP_Camera = fields.Char("Account")
    # passOfIP_Camera = fields.Char("Pass")
    #
    # passOfIP_Camera = fields.Many2one('', 'Quốc gia')
    #
    # author = fields.Many2many('mylib.author', string="Tác giả")
    #
    # descriptionOfIP_Camera = fields.Text("Tiểu sử")