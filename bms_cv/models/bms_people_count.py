# -*- coding: utf-8 -*-

from odoo import fields, models
class People_count(models.Model):
    _name = 'bms_people_count'
    _description = 'People count BMS Computer Vision'

    IP_id = fields.Many2one('bms_computer_vision.ip_camera', string='IP_id')
    time_in_out = fields.Datetime("Thời gian ra vào")
    customers = fields.Boolean("Khách hàng ?")
    in_out = fields.Boolean("in or out ?")
    department = fields.Many2one('hr.department', string='Phòng Ban')