# -*- coding: utf-8 -*-

from odoo import fields, models


class IP_Camera(models.Model):
    _name = 'bms_computer_vision.ip_camera'
    _description = 'IP Camera BMS Computer Vision'

    name = fields.Char("IP")
    accountOfIP_Camera = fields.Char("Account")
    passOfIP_Camera = fields.Char("Pass")
    departmentIdOfIP_Camera = fields.Many2one('hr.department', string="department")
    user = fields.Char("User")
