# -*- encoding: utf-8 -*-
##############################################################################
#
#    Login Thu Vien Dien Tu
#    Copyright (C) 2018- BMSGlobal
##############################################################################

{
    'name': 'Testing Controller',
    'summary': 'Testing Controller For odoo',
    'version': '1.0',
    'category': 'Website',
    'summary': """
Test controller """,
    'author': "BMSER",
    'website': 'http://www.odooglobal.com',
    'license': 'AGPL-3',
    'depends': ['base'
    ],
    'data': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
}
